import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import javax.swing.JFrame;

class GUI extends JPanel {

  public static Graphics2D draw;
  public static JFrame frame;
  
  public static void setup() {
    frame = new JFrame("Pixel Tanks"); 
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(500, 500);
    frame.getContentPane().add(new Panel());
    frame.setVisible(true);
  }

  public static class Panel extends JPanel {
    public void paintComponent(Graphics g) {
      super.paintComponent(g);
      GUI.draw = (Graphics2D) g;
    }
  }
}