//(() => {

  class MegaSocket {
    constructor(url, options) {
      this.url = url;
      this.options = options;
      this.callstack = {
        open: [],
        close: [],
        message: [],
      }
      if (this.options == undefined) {
        this.options = {};
      }
      if (this.options.keepAlive == undefined) {
        this.options.keepAlive = true;
      }
      if (this.options.autoconnect == undefined) {
        this.options.autoconnect = true;
      }
      if (this.options.reconnect == undefined) {
        this.options.reconnect = true;
      }
      if (this.options.autoconnect) {
        this.status = 'connecting';
        this.connect();
      } else {
        this.status = 'idle';
        window.addEventListener('offline', function() { // Arrow Function Wont Work Due To Bind
          this.socket.close();
          this.socket.onclose();
        }.bind(this));
        if (this.options.reconnect) {
          window.addEventListener('online', function() {
            this.connect();
          }.bind(this))
        }
      }
    }
    connect() {
      try {
        this.socket = new WebSocket(this.url);
      } catch (e) {
        console.warn('WebSocket had trouble connecting to ' + this.url);
      }
      this.socket.onopen = function() {
        this.status = 'connected';
        if (this.options.keepAlive) {
          this.socket.keepAlive = setInterval(function() {
            this.socket.send('|');
          }.bind(this), 50000);
        }
        var l = 0;
        while (l < this.callstack.open.length) {
          this.callstack.open[l]();
          l++;
        }
      }.bind(this);
      this.socket.onmessage = function(data) {
        try {
          data = window.jsonpack.unpack(Helper.de(data.data));
        } catch (e) {
          alert('Error converting jsonpack from websocket message: ' + Helper.de(data.data));
        }
        // custom error handling for server
        if (data.status === 'error') {
          alert('WebSocket Error: ' + data.message);
          return;
        }
        // end
        var l = 0;
        while (l < this.callstack.message.length) {
          this.callstack.message[l](data);
          l++;
        }
      }.bind(this);
      this.socket.onclose = function() {
        clearInterval(this.socket.keepAlive);
        this.status = 'disconnected';
        var l = 0;
        while (l < this.callstack.close.length) {
          this.callstack.close[l]();
          l++;
        }
        if (this.options.reconnect) {
          this.connect();
        }
      }.bind(this);
    }
    on(event, operation) {
      if (event == 'connect') {
        this.callstack.open.push(operation);
      }
      if (event == 'message') {
        this.callstack.message.push(operation);
      }
      if (event == 'close') {
        this.callstack.close.push(operation);
      }
    }
    no(event) {
      if (event == 'connect') {
        this.callstack.open = [];
      }
      if (event == 'message') {
        this.callstack.message = [];
      }
      if (event == 'close') {
        this.callstack.close = [];
      }
    }
    send(data) {
      data = Helper.en(window.jsonpack.pack(data));
      this.socket.send(data);
    }
    close() {
      this.socket.close();
    }
  }

  class Helper {
    static en(c) {var x='charCodeAt',b,e={},f=c.split(""),d=[],a=f[0],g=256;for(b=1;b<f.length;b++)c=f[b],null!=e[a+c]?a+=c:(d.push(1<a.length?e[a]:a[x](0)),e[a+c]=g,g++,a=c);d.push(1<a.length?e[a]:a[x](0));for(b=0;b<d.length;b++)d[b]=String.fromCharCode(d[b]);return d.join("")}

    static de(b) {var a,e={},d=b.split(""),c=d[0],f=d[0],g=[c],h=256,o=256;for(b=1;b<d.length;b++)a=d[b].charCodeAt(0),a=h>a?d[b]:e[a]?e[a]:f+c,g.push(a),c=a.charAt(0),e[o]=f+c,o++,f=a;return g.join("")}
  }

  class Menu {
    constructor(draw, listeners, element, context) {
      this.draw = draw.bind(this);
      this.element = element;
      this.listeners = listeners;
      if (context) {
        for (let property in this.listeners) {
          this.listeners[property] = this.listeners[property].bind(context);
        }
      } else {
        for (let property in this.listeners) {
          this.listeners[property] = this.listeners[property].bind(this);
        }
      }
    }

    addListeners() {
      for (let property in this.listeners) {
        this.element.addEventListener(property, this.listeners[property]);
      }
    }
    removeListeners() {
      for (let property in this.listeners) {
        this.element.removeEventListener(property, this.listeners[property]);
      }
    }
  }

  class Menus {
    static trigger(name) {
      if (Menus.current !== undefined) {
        Menus.menus[Menus.current].removeListeners();
      }
      Menus.current = name;
      while (GUI.root.children[0]) {
        GUI.root.removeChild(GUI.root.children[0]);
      }
      Menus.menus[Menus.current].draw();
      Menus.menus[Menus.current].addListeners();
    }

    static onclick(e) {
      var x = (e.clientX-(window.innerWidth-window.innerHeight*1.6)/2)/PixelTanks.resizer;
      var y = e.clientY/PixelTanks.resizer;
      var l = 0;
      while (l < this.data.buttons.length) {
        if (x > this.data.buttons[l].x && x < this.data.buttons[l].x + this.data.buttons[l].w) {
          if (y > this.data.buttons[l].y && y < this.data.buttons[l].y + this.data.buttons[l].h) {
            Menus.trigger(this.data.buttons[l].ref);
          }
        }
        l++;
      }
      var l = 0;
      while (l < this.data.exec.length) {
        if (x > this.data.exec[l].x && x < this.data.exec[l].x + this.data.exec[l].w) {
          if (y > this.data.exec[l].y && y < this.data.exec[l].y + this.data.exec[l].h) {
            eval(this.data.exec[l].ref);
          }
        }
        l++;
      }
    }

    static redraw() {
      while (GUI.root.children[0]) {
        GUI.root.removeChild(GUI.root.children[0]);
      }
      Menus.menus[Menus.current].draw();
    }

    static removeListeners() {
      Menus.menus[Menus.current].removeListeners();
    }

  }

  class Network {
    static get(callback) {
      PixelTanks.socket.send({
        op: 'database',
        type: 'get',
        username: sessionStorage.username,
        token: sessionStorage.token,
      });
      PixelTanks.socket.on('message', (data) => {
        if (data.status === 'success' && data.type === 'get') {
          PixelTanks.socket.no('message');
          callback(JSON.parse(data.data));
        }
      });
    }
    static update(key, value) {
      try {
        PixelTanks.socket.send({
          op: 'database',
          type: 'set',
          username: sessionStorage.username,
          token: sessionStorage.token,
          key: key,
          value: value,
        });
        PixelTanks.socket.on('message', function(data) {
          if (data.success) {
            PixelTanks.socket.no('message');
            console.log('Saved Game Successfully!');
          }
        });
      } catch (e) {}
    }
    static auth(username, password, type, callback) {
      PixelTanks.socket.send({
        op: 'auth',
        type: type,
        username: username,
        password: password,
      });
      PixelTanks.socket.on('message', (data) => {
        if (data.status === 'success') {
          PixelTanks.socket.no('message');
          sessionStorage.username = username;
          sessionStorage.token = data.token;
          callback();
        }
      })
    }
  }

  class GUI {
    
    static resize() {
      PixelTanks.resizer = window.innerHeight/1000;
      GUI.root.height = window.innerHeight;
      GUI.root.width = window.innerHeight*1.6;
      GUI.root.position.set((window.innerWidth-window.innerHeight*1.6)/2, 0);
      GUI.root.scale.set(PixelTanks.resizer);
      GUI.draw.scale.set(PixelTanks.resizer);
      GUI.draw.position.set((window.innerWidth-window.innerHeight*1.6)/2, 0);
      GUI.sidebarA.width = Math.abs((window.innerWidth-window.innerHeight*1.6)/2);
      GUI.sidebarA.height = window.innerHeight;
      GUI.sidebarB.width = Math.abs((window.innerWidth-window.innerHeight*1.6)/2);
      GUI.sidebarB.height = window.innerHeight;
      GUI.sidebarB.x = window.innerWidth-GUI.sidebarB.width;
      Menus.redraw();
    }

    static resetSpritePool() {
      GUI.nextsprite = 0;
    }

    static resetTextPool() {
      GUI.nexttext = 0;
    }

    static drawImage(image, x, y, w, h, a) {
      var sprite = GUI.spritepool[GUI.nextsprite];
      sprite.texture = image;
      sprite.x = x;
      sprite.y = y;
      sprite.width = w;
      sprite.height = h;
      sprite.alpha = a;
      sprite.pivot.x = 0;
      sprite.pivot.y = 0;
      sprite.angle = 0;
      GUI.root.addChild(sprite);
      GUI.nextsprite++;
      if (GUI.nextsprite === GUI.spritepool.length) GUI.resetSpritePool();
    }

    static drawImageRotate(image, x, y, w, h, px, py, r, a) {
      var sprite = GUI.spritepool[GUI.nextsprite];
      sprite.texture = image;
      sprite.x = x;
      sprite.y = y;
      sprite.width = w;
      sprite.height = h;
      sprite.pivot.x = px;
      sprite.pivot.y = py;
      sprite.angle = r;
      sprite.alpha = a;
      GUI.root.addChild(sprite);
      GUI.nextsprite++;
      if (GUI.nextsprite === GUI.spritepool.length) GUI.resetSpritePool();
    }

    static drawText(message, x, y, size, color, anchor) {
      var text = GUI.textpool[GUI.nexttext];
      text.text = message;
      text.style = {
        fill: color,
        fontFamily: 'Neuropol',
        fontWeight: 300,
        fontSize: size,
      };
      text.anchor.set(anchor);
      text.x = x;
      text.y = y;
      GUI.nexttext++;
      GUI.root.addChild(text);
      if (GUI.nexttext === GUI.textpool.length) GUI.resetTextPool();
    }

    static clear() {
      while (GUI.root.children[0]) {
        GUI.root.removeChild(GUI.root.children[0]);
      }
      GUI.draw.clear();
    }
  }

  class PixelTanks {

    static start() {
      PixelTanks.setup();
      PixelTanks.boot() // Load all variables, images, sockets, and other stuff for launch
    }

    static setup() {
      PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
      const ticker = PIXI.Ticker.shared;
      ticker.autoStart = false;
      ticker.stop();
      GUI.spritepool = [];
      GUI.textpool = [];
      var l = 0;
      while (l<300) {
        GUI.spritepool.push(new PIXI.Sprite());
        l++;
      }
      l = 0;
      while (l<10) {
        GUI.textpool.push(new PIXI.Text());
        l++;
      }
      GUI.nextsprite = 0;
      GUI.nexttext = 0;
      GUI.app = new PIXI.Application({
        resizeTo: window,
        backgroundColor: '#000000',
        antialias: true,
      });
      document.body.appendChild(GUI.app.view);

      PixelTanks.resizer = window.innerHeight/1000;
      GUI.root = new PIXI.Container();
      GUI.root.height = window.innerHeight;
      GUI.root.width = window.innerHeight*1.6;
      GUI.root.position.set((window.innerWidth-window.innerHeight*1.6)/2, 0);
      GUI.root.scale.set(PixelTanks.resizer);
      GUI.app.stage.addChild(GUI.root);
      GUI.draw = new PIXI.Graphics();
      GUI.draw.scale.set(PixelTanks.resizer);
      GUI.draw.position.set((window.innerWidth-window.innerHeight*1.6)/2, 0);
      GUI.app.stage.addChild(GUI.draw);
      GUI.sidebarA = PIXI.Sprite.from(PIXI.Texture.WHITE);
      GUI.sidebarA.width = Math.abs((window.innerWidth-window.innerHeight*1.6)/2);
      GUI.sidebarA.height = window.innerHeight;
      GUI.sidebarA.x = 0
      GUI.sidebarA.y = 0;
      GUI.sidebarA.zIndex = 1000;
      GUI.sidebarA.tint = 0x000000;
      GUI.app.stage.addChild(GUI.sidebarA);
      GUI.sidebarB = PIXI.Sprite.from(PIXI.Texture.WHITE);
      GUI.sidebarB.width = Math.abs((window.innerWidth-window.innerHeight*1.6)/2);
      GUI.sidebarB.height = window.innerHeight;
      GUI.sidebarB.x = window.innerWidth-GUI.sidebarB.width;
      GUI.sidebarB.y = 0;
      GUI.sidebarB.zIndex = 1000;
      GUI.sidebarB.tint = 0x000000;
      GUI.app.stage.addChild(GUI.sidebarB);

      GUI.drawText('0%', 800, 500, 50, '#ffffff', 0.5);
    }

    static updateBootProgress(progress) {
      GUI.clear();
      GUI.drawText(Math.round(progress*100)+'%', 800, 500, 50, '#ffffff', 0.5);
    }

    static async boot() {
      
      // variable setup
      PixelTanks.b = []; // Blocks
      PixelTanks.s = []; // Bullets
      PixelTanks.ai = []; // Enemies
      PixelTanks.texturepack = 'default'; // texturepack
      // setup sprite map
      PixelTanks.images = { // mapping
        blocks: {
          default: {
            barrier: '/images/blocks/default/barrier.png',
            strong: '/images/blocks/default/strong.png',
            weak: '/images/blocks/default/weak.png',
            spike: '/images/blocks/default/spike.png',
            floor: '/images/blocks/default/floor.png',
            void: '/images/blocks/default/void.png',
            gold: '/images/blocks/default/gold.png',
            heal: '/images/blocks/default/heal.png',
            mine: '/images/blocks/default/mine.png',
            airstrike: '/images/blocks/default/airstrike.png',
            fortress: '/images/blocks/fortress.png',
          },
        },
        bullets: {
          default: {
            // normal: '/images/bullets/default/normal.png', no image yet :(
            shotgun: '/images/bullets/default/shotgun.png',
            condensed: '/images/bullets/default/condensed.png',
            powermissle: '/images/bullets/default/powermissle.png',
            megamissle: '/images/bullets/default/megamissle.png',
            grapple: '/images/bullets/default/grapple.png',
            explosion: '/images/bullets/default/explosion.png',
            dynamite: '/images/bullets/default/dynamite.png',
          },
        },
        tanks: {
          default: {
            other: {
              buff: '/images/tanks/default/other/buff.png',
            },
            red: {
              top: '/images/tanks/default/red/top.png',
              bottom: [
                '/images/tanks/default/red/bottom.png',
                '/images/tanks/default/red/bottom2.png',
              ],
              base: '/images/tanks/default/red/base.png'
            },
            iron: {
              top: '/images/tanks/default/iron/top.png',
              bottom: [
                '/images/tanks/default/iron/bottom.png',
                '/images/tanks/default/iron/bottom2.png',
              ],
            },
            diamond: {
              top: '/images/tanks/default/diamond/top.png',
              bottom: [
                '/images/tanks/default/diamond/bottom.png',
                '/images/tanks/default/diamond/bottom2.png',
              ],
            },
            dark: {
              top: '/images/tanks/default/dark/top.png',
              bottom: [
                '/images/tanks/default/dark/bottom.png',
                '/images/tanks/default/dark/bottom2.png',
              ],
            },
            light: {
              top: '/images/tanks/default/light/top.png',
              bottom: [
                '/images/tanks/default/light/bottom.png',
                '/images/tanks/default/light/bottom2.png',
              ],
            },
            cosmetics: [{
              name: 'Aaron',
              
              id: 'aaron',
            }, {
              name: 'Astronaut',
              
              id: 'astronaut'
            }, {
              name: 'Onfire',
              
              id: 'onfire'
            }, {
              name: 'Assassin',
              
              id: 'assassin'
            }, {
              name: 'Redsus',
              
              id: 'redsus'
            }, {
              name: 'Venom',
              
              id: 'venom',
            }, {
              name: 'Blue Tint',
              rarity: 0,
              price: 0,
              
              id: 'blue_tint',
            }, {
              name: 'Purple Flower',
              rarity: 0,
              price: 0,
              
              id: 'purple_flower',
            }, {
              name: 'Leaf',
              rarity: 0,
              price: 0,
              
              id: 'leaf',
            }, {
              name: 'Basketball',
              rarity: 0,
              price: 0,
              
              id: 'basketball',
            }, {
              name: 'Purple Top Hat',
              rarity: 0,
              price: 0,
              
              id: 'purple_top_hat',
            }, {
              name: 'Terminator',
              rarity: 0,
              price: 0,
              
              id: 'terminator',
            }, {
              name: 'Dizzy',
              rarity: 0,
              price: 0,
              
              id: 'dizzy',
            }, {
              name: 'Knife',
              rarity: 0,
              price: 0,
              
              id: 'knife',
            }, {
              name: 'Scared',
              rarity: 0,
              price: 0,
              
              id: 'scared',
            }, {
              name: 'Laff',
              rarity: 0,
              price: 0,
              
              id: 'laff',
            }, {
              name: 'Hacker Hoodie',
              rarity: 0,
              price: 0,
              
              id: 'hacker_hoodie',
            }, {
              name: 'Error',
              rarity: 0,
              price: 0,
              
              id: 'error',
            }, {
              name: 'Purple Grad Hat',
              rarity: 0,
              price: 0,
              
              id: 'purple_grad_hat',
            }, {
              name: 'Bat Wings',
              rarity: 0,
              price: 0,
              
              id: 'bat_wings',
            }, {
              name: 'Back Button',
              rarity: 0,
              price: 0,
              
              id: 'back',
            }, {
              name: 'Fisher Hat',
              rarity: 0,
              price: 0,
              
              id: 'fisher_hat',
            }, {
              name: 'Ban',
              rarity: 0,
              price: 0,
              
              id: 'ban',
            }, {
              name: 'Blue Ghost',
              rarity: 0,
              price: 0,
              
              id: 'blue_ghost',
            }, {
              name: 'Pumpkin Face',
              rarity: 0,
              price: 0,
              
              id: 'pumpkin_face',
            }, {
              name: 'Pumpkin Hat',
              rarity: 0,
              price: 0,
              
              id: 'pumpkin_hat',
            }, {
              name: 'Red Ghost',
              rarity: 0,
              price: 0,
              
              id: 'red_ghost',
            }, {
              name: 'Candy Corn',
              rarity: 0,
              price: 0,
              
              id: 'candy_corn',
            }, {
              name: 'Yellow Pizza',
              rarity: 0,
              price: 0,
              
              id: 'yellow_pizza',
            }, {
              name: 'Orange Ghost',
              rarity: 0,
              price: 0,
              
              id: 'orange_ghost',
            }, {
              name: 'Pink Ghost',
              rarity: 0,
              price: 0,
              
              id: 'pink_ghost',
            }, {
              name: 'Paleontologist',
              rarity: 0,
              price: 0,
              
              id: 'paleontologist',
            }, {
              name: 'Yellow Hoodie',
              rarity: 0,
              price: 0,
              
              id: 'yellow_hoodie',
            }, {
              name: 'X',
              rarity: 0,
              price: 0,
              
              id: 'x',
            }, {
              name: 'Sweat',
              rarity: 0,
              price: 0,
              
              id: 'sweat',
            }, {
              name: 'Spirals',
              rarity: 0,
              price: 0,
              
              id: 'spirals',
            }, {
              name: 'Spikes',
              rarity: 0,
              price: 0,
              
              id: 'spikes',
            }, {
              name: 'Rudolph',
              rarity: 0,
              price: 0,
              
              id: 'rudolph',
            }, {
              name: 'Reindeer Hat',
              rarity: 0,
              price: 0,
              
              id: 'reindeer_hat',
            }, {
              name: 'Red Hoodie',
              rarity: 0,
              price: 0,
              
              id: 'red_hoodie',
            }, {
              name: 'Question Mark',
              rarity: 0,
              price: 0,
              
              id: 'question_mark',
            }, {
              name: 'Pink/Purple Hoodie',
              rarity: 0,
              price: 0,
              
              id: 'purplepink_hoodie',
            }, {
              name: 'Purple Hoodie',
              rarity: 0,
              price: 0,
              
              id: 'purple_hoodie',
            }, {
              name: 'Pumpkin',
              rarity: 0,
              price: 0,
              
              id: 'pumpkin',
            }, {
              name: 'Pickle',
              rarity: 0,
              price: 0,
              
              id: 'pickle',
            }, {
              name: 'Orange Hoodie',
              rarity: 0,
              price: 0,
              
              id: 'orange_hoodie',
            }, {
              name: 'Helment',
              rarity: 0,
              price: 0,
              
              id: 'helment',
            }, {
              name: 'Green Hoodie',
              rarity: 0,
              price: 0,
              
              id: 'green_hoodie',
            }, {
              name: 'Exclaimation Point',
              rarity: 0,
              price: 0,
              
              id: 'exclaimation_point',
            }, {
              name: 'Eggplant',
              rarity: 0,
              price: 0,
              
              id: 'eggplant',
            }, {
              name: 'Devils Wings',
              rarity: 0,
              price: 0,
              
              id: 'devils_wings',
            }, {
              name: 'Christmas Tree',
              rarity: 0,
              price: 0,
              
              id: 'christmas_tree',
            }, {
              name: 'Christmas Lights',
              rarity: 0,
              price: 0,
              
              id: 'christmas_lights',
            }, {
              name: 'Checkmark',
              rarity: 0,
              price: 0,
              
              id: 'checkmark',
            }, {
              name: 'Cat Hat',
              rarity: 0,
              price: 0,
              
              id: 'cat_hat',
            }, {
              name: 'Blueberry',
              rarity: 0,
              price: 0,
              
              id: 'blueberry',
            }, {
              name: 'Blue Hoodie',
              rarity: 0,
              price: 0,
              
              id: 'blue_hoodie',
            }, {
              name: 'Blue Helment',
              rarity: 0,
              price: 0,
              
              id: 'blue_helment',
            }, {
              name: 'Banana',
              rarity: 0,
              price: 0,
              
              id: 'bannana',
            }, {
              name: 'Aqua Helment',
              rarity: 0,
              price: 0,
              
              id: 'aqua_helment',
            }, {
              name: 'Apple',
              rarity: 0,
              price: 0,
              
              id: 'apple',
            }, {
              name: 'Hoodie',
              rarity: 20,
              price: 1000000,
              
              id: 'hoodie',
            }, {
              name: 'Purple Helment',
              rarity: 20,
              price: 1000000,
              
              id: 'purple_helment',
            }, {
              name: 'Angel Wings',
              rarity: 4,
              price: 20000,
              
              id: 'angel_wings',
            }, {
              name: 'Boost',
              rarity: 4,
              price: 20000,
              
              id: 'boost',
            }, {
              name: 'Bunny Ears',
              rarity: 2,
              price: 1000,
              
              id: 'bunny_ears'
            }, {
              name: 'Cake',
              rarity: 4,
              price: 4000,
              
              id: 'cake',
            }, {
              name: 'Cancelled',
              rarity: 1,
              price: 1000,
              
              id: 'cancelled',
            }, {
              name: 'Candy Cane',
              rarity: 5,
              price: 1000,
              
              id: 'candy_cane',
            }, {
              name: 'Cat Ears',
              rarity: 4,
              price: 20000,
              
              id: 'cat_ears',
            }, {
              name: 'Christmas Hat',
              rarity: 8,
              price: 30000,
              
              id: 'christmas_hat',
            }, {
              name: 'Controller',
              rarity: 10,
              price: 1000000,
              
              id: 'controller',
            }, {
              name: 'Deep Scratch',
              rarity: 4,
              price: 20000,
              
              id: 'deep_scratch',
            }, {
              name: 'Devils Horns',
              rarity: 4,
              price: 20000,
              
              id: 'devil_horn',
            }, {
              name: 'Headphones',
              rarity: 4,
              price: 20000,
              
              id: 'earmuffs',
            }, {
              name: 'Eyebrows',
              rarity: 4,
              price: 20000,
              
              id: 'eyebrows',
            }, {
              name: 'First Aid',
              rarity: 4,
              price: 10000,
              
              id: 'first_aid',
            }, {
              name: 'Flag',
              rarity: 4,
              price: 20000,
              
              id: 'flag',
            }, {
              name: 'Halo',
              rarity: 1,
              price: 5000,
              
              id: 'halo',
            }, {
              name: 'Hacks',
              rarity: 10,
              price: 1000000,
              
              id: 'hax',
            }, {
              name: 'Low Battery',
              rarity: 5,
              price: 50000,
              
              id: 'low_battery',
            }, {
              name: 'Mini Tank',
              rarity: 8,
              price: 20000,
              
              id: 'mini_tank',
            }, {
              name: 'MLG Glasses',
              rarity: 10,
              price: 100000,
              
              id: 'mlg_glasses',
            }, {
              name: 'Money Eyes',
              rarity: 2,
              price: 10000,
              
              id: 'money_eyes',
            }, {
              name: 'No Mercy',
              rarity: 4,
              price: 2000,
              
              id: 'no_mercy',
            }, {
              name: 'Peace',
              rarity: 4,
              price: 2000,
              
              id: 'peace',
            }, {
              name: 'Police',
              rarity: 4,
              price: 2000,
              
              id: 'police',
            }, {
              name: 'Question Mark',
              rarity: 4,
              price: 2000,
              
              id: 'question_mark',
            }, {
              name: 'Rage',
              rarity: 4,
              price: 20000,
              
              id: 'rage',
            }, {
              name: 'Small Scratch',
              rarity: 4,
              price: 20000,
              
              id: 'small_scratch',
            }, {
              name: 'Speaker',
              rarity: 2,
              price: 2000,
              
              id: 'speaker',
            }, {
              name: 'Swords',
              rarity: 4,
              price: 20000,
              
              id: 'swords',
            }, {
              name: 'Tools',
              rarity: 4,
              price: 2000,
              
              id: 'tools',
            }, {
              name: 'Top Hat',
              
              id: 'top_hat',
            }, {
              name: 'Uno Reverse',
              
              id: 'uno_reverse',
            }, {
              name: 'Victim',
              
              id: 'victim',
            }],
          },
          // more texture packs
        },
        menus: {
          default: {
            ui: '/images/menus/default/ui.png',
            start: '/images/menus/default/start.png',
            main: '/images/menus/default/main.png',
            multiplayer: '/images/menus/default/multiplayer.png',
            /*levelSelect: 'data:image/png;base64,',
            levelSelect2: 'data:image/png;base64,',              Menus are generated by canvas not image :)
            levelSelect3: 'data:image/png;base64,',
            levelSelect4: 'data:image/png;base64,',*/
            pvp: '/images/menus/default/pvp.png',
            co_op: '/images/menus/default/co_op.png',
            ffa: '/images/menus/default/ffa.png',
            duels: '/images/menus/default/duels.png',
            crate: '/images/menus/default/cosmetic_main.png',
            cosmeticLeft: '/images/menus/default/cosmetics_left.png',
            cosmeticCenter: '/images/menus/default/cosmetics_middle.png',
            cosmeticRight: '/images/menus/default/cosmetics_right.png',
            defeat: '/images/menus/default/defeat.png',
            victory: '/images/menus/default/victory.png',
            settings: '/images/menus/default/settings.png',
            keybinds: '/images/menus/default/keybinds.png',
            inventory: '/images/menus/default/inventory.png',
            classTab: '/images/menus/default/classTab.png',
            healthTab: '/images/menus/default/healthTab.png',
            itemTab: '/images/menus/default/itemTab.png',
            //shop: '/images/menus/default/shop.png',
            shop: '/images/menus/default/new-shop.png',
            itemshop: '/images/menus/default/item-shop.png',
            armorshop: '/images/menus/default/armor-shop.png',
            classshop: '/images/menus/default/class-shop.png',
            banged: '/images/menus/default/banged.png',
          },
        },
        emotes: { // type: 0=loop 1=play once 2=static
          speech: {
            image: '/images/emotes/speech.png',
            speed: 50,
          },
          mlg: {
            image: '/images/emotes/mlg.png',
            type: 1,
            frames: 13,
            speed: 50,
          },
          wink: {
            image: '/images/emotes/wink.png',
            type: 2,
            speed: 50,
          },
          confuzzled: {
            image: '/images/emotes/confuzzled.png',
            type: 2,
            speed: 50,
          },
          surrender: {
            image: '/images/emotes/surrender.png',
            type: 2,
            speed: 50,
          },
          anger: {
            image: '/images/emotes/anger.png',
            type: 0,
            frames: 4,
            speed: 50,
          },
          ded: {
            image: '/images/emotes/ded.png',
            type: 2,
            speed: 50,
          },
          mercy: {
            image: '/images/emotes/mercy.png',
            type: 0,
            frames: 1,
            speed: 50,
          },
          suffocation: {
            image: '/images/emotes/suffocation.png',
            type: 0,
            frames: 3,
            speed: 50,
          },
          nomercy: {
            image: '/images/emotes/nomercy.png',
            type: 0,
            frames: 1,
            speed: 50,
          },
          idea: {
            image: '/images/emotes/idea.png',
            type: 1,
            frames: 6,
            speed: 50,
          },
          scared: {
            image: '/images/emotes/scared.png',
            type: 2,
            speed: 50,
          },
          crying: {
            image: '/images/emotes/crying.png',
            type: 0,
            frames: 5,
            speed: 50,
          },
          flat: {
            image: '/images/emotes/flat.png',
            type: 0,
            frames: 1,
            speed: 50,
          },
          noflat: {
            image: '/images/emotes/noflat.png',
            type: 0,
            frames: 1,
            speed: 50,
          },
          rage: {
            image: '/images/emotes/rage.png',
            type: 0,
            frames: 5,
            speed: 50,
          },
          sad: {
            image: '/images/emotes/sad.png',
            type: 0,
            frames: 2,
            speed: 50,
          },
          sweat: {
            image: '/images/emotes/sweat.png',
            type: 0,
            frames: 10,
            speed: 50,
          },
          teamedon: {
            image: '/images/emotes/miss.png',
            type: 1,
            frames: 28,
            speed: 75,
          },
          evanism: {
            image: '/images/emotes/evanism.png',
            type: 1,
            frames: 45,
            speed: 100,
          },
          miss: {
            image: '/images/emotes/teamedon.png',
            type: 0,
            frames: 12,
            speed: 50,
          }
        }, // REVISE make loader more optimized (fix repeating /images/emotes/)
        animations: {
          tape: {
            image: '/images/animations/tape.png',
            frames: 17,
            speed: 50,
          },
          toolkit: {
            image: '/images/animations/toolkit.png',
            frames: 16,
            speed: 50,
          },
          /*glu: {
            image: '/images/animations/glu.png',
            frames: 0,
            speed: 1000,
          },*/
        },
        items: {
          airstrike: '/images/items/airstrike.png',
          duck_tape: '/images/items/duck-tape.png',
          super_glu: '/images/items/super-glu.png',
          shield: '/images/items/shield.png',
          flashbang: '/images/items/flashbang.png',
          bomb: '/images/items/bomb.png',
          dynamite: '/images/items/dynamite.png',
          power_bomb: '/images/items/power-bomb.png',
          weak: '/images/items/weak.png',
          strong: '/images/items/strong.png',
          spike: '/images/items/spike.png',
          mine: '/images/items/mine.png',
          fortress: '/images/items/fortress.png',
        }
      };

      Menus.menus = {
        start: '',
        main: '',
        multiplayer: '',
        levelSelect: '',
        levelSelect2: '',
        levelSelect3: '',
        levelSelect4: '',
        pvp: '',
        co_op: '',
        ffa: '',
        duels: '',
        crate: '',
        cosmetic: '',
        settings: '',
        keybinds: '',
        inventory: '',
        shop: '',
        defeat: '',
        victory: '',
        shop: '',
        itemshop: '',
        armorshop: '',
        classshop: '',
      }

      PixelTanks.crates = {
        red: [{
          name: 'Apple',
          rarity: 'uncommon',
        }, {
          name: 'Redsus',
          rarity: 'legendary',
        }, {
          name: 'Rage',
          rarity: 'epic',
        }, {
          name: 'X',
          rarity: 'common',
        }, {
          name: 'Hacks',
          rarity: 'rare',
        }, {
          name: 'Red Hoodie',
          rarity: 'common',
        }, {
          name: 'Devils Wings',
          rarity: 'common',
        }, {
          name: 'Devils Horns',
          rarity: 'common',
        }, {
          name: 'Exclaimation Point',
          rarity: 'common',
        }],
        orange: [{
          name: 'Pumpkin',
          rarity: 'uncommon',
        }, {
          name: 'Onfire',
          rarity: 'epic',
        }, {
          name: 'Orange Hoodie',
          rarity: 'common',
        }, {
          name: 'Tools',
          rarity: 'rare',
        }, {
          name: 'Basketball',
          rarity: 'uncommon',
        }, ],
        yellow: [{
          name: 'Yellow Hoodie',
          rarity: 'common',
        }, {
          name: 'Banana',
          rarity: 'uncommon',
        }, {
          name: 'Halo',
          rarity: 'epic',
        }, {
          name: 'Uno Reverse',
          rarity: 'legendary',
        }, {
          name: 'Money Eyes',
          rarity: 'rare',
        }, {
          name: 'Dizzy',
          rarity: 'dizzy',
        }],
        green: [{
          name: 'Pickle',
          rarity: 'uncommon',
        }, {
          name: 'Checkmark',
          rarity: 'rare',
        }, {
          name: 'Green Hoodie',
          rarity: 'common',
        }, {
          name: 'Leaf',
          rarity: 'common',
        }],
        blue: [{
          name: 'Blue Hoodie',
          rarity: 'common',
        }, {
          name: 'Police',
          rarity: 'epic',
        }, {
          name: 'Blueberry',
          rarity: 'uncommon',
        }, {
          name: 'Sweat',
          rarity: 'rare',
        }, {
          name: 'Scared',
          rarity: 'rare',
        }, {
          name: 'Blue Tint',
          rarity: 'rare',
        }],
        purple: [{
          name: 'Purple Hoodie',
          rarity: 'common',
        }, {
          name: 'Eggplant',
          rarity: 'uncommon',
        }, {
          name: 'Purple Flower',
          rarity: 'common',
        }, {
          name: 'Purple Top Hat',
          rarity: 'rare',
        }, {
          name: 'Purple Grad Hat',
          rarity: 'rare',
        }],
        mark: [{
          name: 'Boost',
          rarity: 'common',
        }, {
          name: 'Peace',
          rarity: 'uncommon',
        }, {
          name: 'Question Mark',
          rarity: 'uncommon',
        }, {
          name: 'Cancelled',
          rarity: 'common',
        }, {
          name: 'Eyebrows',
          rarity: 'rare',
        }, {
          name: 'Small Scratch',
          rarity: 'uncommon',
        }, {
          name: 'Deep Scratch',
          rarity: 'epic',
        }, {
          name: 'Spirals',
          rarity: 'common',
        }, {
          name: 'Laff',
          rarity: 'common',
        }, {
          name: 'Ban',
          rarity: 'uncommon',
        }, {
          name: 'Back',
          rarity: 'epic',
        }],
        grey: [{
          name: 'Headphones',
          rarity: 'uncommon',
        }, {
          name: 'Assassin',
          rarity: 'epic',
        }, {
          name: 'Astronaut',
          rarity: 'epic',
        }, {
          name: 'Helment',
          rarity: 'rare',
        }, {
          name: 'Speaker',
          rarity: 'common',
        }, {
          name: 'Spikes',
          rarity: 'common',
        }, {
          name: 'Controller',
          rarity: 'epic',
        }, {
          name: 'Bat Wings',
          rarity: 'common',
        }, {
          name: 'Terminator',
          rarity: 'legendary',
        }, {
          name: 'Knife',
          rarity: 'uncommon',
        }],
        christmas: [{
          name: 'Rudolph',
          rarity: 'rare',
        }, {
          name: 'Christmas Hat',
          rarity: 'legendary',
        }, {
          name: 'Christmas Tree',
          rarity: 'common',
        }, {
          name: 'Christmas Lights',
          rarity: 'epic',
        }, {
          name: 'Candy Cane',
          rarity: 'common',
        }, {
          name: 'Reindeer Hat',
          rarity: 'uncommon',
        }],
        halloween: [{
          name: 'Pumpkin Hat',
          rarity: 'uncommon',
        }, {
          name: 'Pumpkin Face',
          rarity: 'common',
        }, {
          name: 'Candy Corn',
          rarity: 'rare',
        }, {
          name: 'Cat Ears',
          rarity: 'uncommon',
        }, ],
        misc: [{
          name: 'MLG Glasses',
          rarity: 'mythic',
        }, {
          name: 'Cake',
          rarity: 'uncommon',
        }, {
          name: 'Cat Hat',
          rarity: 'uncommon',
        }, {
          name: 'Flag',
          rarity: 'rare',
        }, {
          name: 'Mini Tank',
          rarity: 'legendary',
        }, {
          name: 'No Mercy',
          rarity: 'epic',
        }, {
          name: 'Swords',
          rarity: 'rare',
        }, {
          name: 'Top Hat',
          rarity: 'common',
        }, {
          name: 'Victim',
          rarity: 'common',
        }, {
          name: 'First Aid',
          rarity: 'uncommon',
        }, {
          name: 'Pink/Purple Hoodie',
          rarity: 'common',
        }, {
          name: 'Paleontologist',
          rarity: 'legendary',
        }, {
          name: 'Bunny Ears',
          rarity: 'common',
        }, {
          name: 'Fisher Hat',
          rarity: 'uncommon',
        }, {
          name: 'Error',
          rarity: 'epic',
        }],
        pizza: [{
          name: 'Red Ghost',
          rarity: 'common',
        }, {
          name: 'Blue Ghost',
          rarity: 'common',
        }, {
          name: 'Pink Ghost',
          rarity: 'common',
        }, {
          name: 'Orange Ghost',
          rarity: 'common',
        }, {
          name: 'Yellow Pizza',
          rarity: 'legendary',
        }],
      };

      var menuActions = {
        start: {
          buttons: [],
          exec: [{
            x: 580,
            y: 740,
            w: 200,
            h: 100,
            ref: `(() => {
            Network.auth(Menus.menus.start.username, Menus.menus.start.password, 'login', () => {
              PixelTanks.user = {};
              PixelTanks.user.username = sessionStorage.username;
              Network.get((data) => {
                PixelTanks.userData = JSON.parse(data.playerdata)['pixel-tanks'];
                PixelTanks.playerData = JSON.parse(data.playerdata);
                if (!PixelTanks.userData) {
                  PixelTanks.userData = {
                    username: sessionStorage.username,
                    material: 0,
                    coins: 0,
                    scraps: 0,
                    crates: 1,
                    level: 1,
                    xp: 0,
                    rank: 1,
                    blocks: 5,
                    flashbangs: 5,
                    boosts: 5,
                    toolkits: 5,
                    class: 'normal',
                    stealth: false,
                    builder: false,
                    tactical: false,
                    summoner: false,
                    steel: false,
                    crystal: false,
                    dark: false,
                    light: false,
                    cosmetic: '',
                    cosmetics: [],
                    item1: 'toolkit',
                    item2: 'weak',
                    item3: 'bomb',
                    item4: 'grapple',
                    settings: {
                      item1: 49,
                      item2: 50,
                      item3: 51,
                      item4: 52,
                    },
                  };
                }
                Menus.trigger('main');
              });
            });
          })();`,
          }, {
            x: 820,
            y: 740,
            w: 200,
            h: 100,
            ref: `(() => {
            Network.auth(Menus.menus.start.username, Menus.menus.start.password, 'signup', () => {
              PixelTanks.user = {};
              PixelTanks.user.username = sessionStorage.username;
              PixelTanks.userData = {
                username: sessionStorage.username,
                material: 0,
                coins: 0,
                level: 1,
                blocks: 0,
                flashbangs: 0,
                boosts: 0,
                toolkits: 0,
                class: 'normal',
                stealth: false,
                builder: false,
                tactical: false,
                summoner: false,
                steel: false,
                crystal: false,
                dark: false,
                light: false,
                cosmetic: '',
                cosmetics: [],
                xp: 0,
                rank: 1,
                crates: 0,
                item1: 'toolkit',
                item2: 'weak',
                item3: 'duck_tape',
                item4: 'flashbang',
                material: 0,
                settings: {
                  item1: 49,
                  item2: 50,
                  item3: 51,
                  item4: 52,
                },
              };
              Menus.trigger('main');
            })
          })();`
          }, {
            x: 580,
            y: 480,
            w: 440,
            h: 60,
            ref: `(() => {
            Menus.menus.start.type = 'username';
          })();`
          }, {
            x: 580,
            y: 580,
            w: 440,
            h: 60,
            ref: `(() => {
            Menus.menus.start.type = 'password';
          })();`
          }],
          listeners: {
            keydown: (e) => {
              var start = Menus.menus.start;
              if (e.key.length === 1) {
                start[start.type] += e.key;
              } else if (e.keyCode === 8) {
                start[start.type] = start[start.type].slice(0, -1);
              }
              Menus.redraw();
            }
          },
          customOnLoad: () => {
            if (!Menus.menus.start.type) {
              if (sessionStorage.username !== undefined) {
                PixelTanks.user = {};
                PixelTanks.user.username = sessionStorage.username;
                Network.get((data) => {
                  PixelTanks.userData = JSON.parse(data.playerdata)['pixel-tanks'];
                  PixelTanks.playerData = JSON.parse(data.playerdata);
                  Menus.trigger('main');
                });
              } 
              Menus.menus.start.type = 'username';
              Menus.menus.start.username = '';
              Menus.menus.start.password = '';
            }
            GUI.drawText(Menus.menus.start.username, 580, 495, 30, '#ffffff', 0)
            var l = 0, temp = '';
            while (l < Menus.menus.start.password.length) {
              temp += '*';
              l++;
            }
            GUI.drawText(temp, 580, 595, 30, '#ffffff', 0);
          },
        },
        main: {
          buttons: [{
            x: 1180,
            y: 920,
            w: 80,
            h: 80,
            ref: 'settings',
          }, {
            x: 580,
            y: 500,
            w: 440,
            h: 100,
            ref: 'multiplayer',
          }, {
            x: 580,
            y: 360,
            w: 440,
            h: 100,
            ref: 'levelSelect',
          }, {
            x: 580,
            y: 640,
            w: 440,
            h: 100,
            ref: 'shop',
          }, {
            x: 420,
            y: 920,
            w: 80,
            h: 80,
            ref: 'inventory',
          }],
          exec: [{
            x: 320,
            y: 920,
            w: 80,
            h: 80,
            ref: `(() => {
              sessionStorage.token = undefined;
              sessionStorage.username = undefined;
              Menus.trigger('start');
            })();`
          }],
          listeners: {},
          customOnLoad: () => {
            PixelTanks.save();

            var key = {
              0: 'red',
              1: 'iron',
              2: 'diamond',
              3: 'dark',
              4: 'light',
            };

            GUI.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[PixelTanks.userData.material]].bottom[0], 250, 400, 80, 80, 1);
            GUI.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[PixelTanks.userData.material]].top, 250, 400, 80, 90, 1);
            if (PixelTanks.userData.cosmetic != '' || PixelTanks.userData.cosmetic != undefined) {
              var l = 0;
              while (l < PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
                if (PixelTanks.userData.cosmetic == PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].name) {
                  GUI.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].image, 250, 400, 80, 90, 1);
                  break; // CLEANUP is this faster?????
                }
                l++;
              }
            }
            GUI.drawText(PixelTanks.user.username, 300, 420, 30, '#ffffff', 0.5)

            var xpToLevelUp = Math.ceil(Math.pow(1.6, PixelTanks.userData.rank - 1) + 20 * (PixelTanks.userData.rank - 1));
            while (PixelTanks.userData.xp >= xpToLevelUp) {
              PixelTanks.userData.xp -= xpToLevelUp;
              PixelTanks.userData.rank += 1;
              xpToLevelUp = Math.ceil(Math.pow(1.6, PixelTanks.userData.rank - 1) + 20 * (PixelTanks.userData.rank - 1));
            }
            GUI.drawText('Rank: ' + PixelTanks.userData.rank, 300, 440, '#ffffff', 0);
            GUI.drawText('XP - ' + PixelTanks.userData.xp + '/' + xpToLevelUp, 300, 460, '#ffffff', 0);
            //GUI.drawImage(coins, 300, 100, 100, 100);
            GUI.drawText(PixelTanks.userData.coins, 300, 480, '#ffffff', 0);
          },
        },
        multiplayer: {
          buttons: [{
            x: 58,
            y: 361,
            w: 188,
            h: 83,
            ref: 'co_op'
          }, {
            x: 255,
            y: 361,
            w: 188,
            h: 83,
            ref: 'pvp',
          }, {
            x: 59,
            y: 56,
            w: 53,
            h: 53,
            ref: 'main',
          }],
          exec: [],
          listeners: {},
          customOnLoad: () => {} // Server Data?
        },
        levelSelect: {
          buttons: [],
          exec: [],
          listeners: {},
          customOnLoad: () => {
            GUI.draw.fillStyle = '#556B2f';
            GUI.draw.fillRect(50, 50, 400, 400);
            GUI.draw.fillStyle = '#ffffff';
            GUI.draw.fillRect(139, 94, 44, 44);
            if (PixelTanks.userData.level < 2) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94, 44, 44);
            if (PixelTanks.userData.level < 3) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94, 44, 44);
            if (PixelTanks.userData.level < 4) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 5) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 6) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 7) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 8) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 9) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 10) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 6, 44, 44);
            if (PixelTanks.userData.level < 11) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 6, 44, 44);
            if (PixelTanks.userData.level < 12) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 6, 44, 44);
            GUI.draw.fillStyle = '#000000';
            GUI.draw.font = '20px Neuropol';
            GUI.draw.fillText('1', 139 + 15, 94 + 30);
            if (PixelTanks.userData.level < 2) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('2', 228 + 15, 94 + 30);
            if (PixelTanks.userData.level < 3) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('3', 317 + 15, 94 + 30);
            if (PixelTanks.userData.level < 4) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('4', 139 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 5) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('5', 228 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 6) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('6', 317 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 7) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('7', 139 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 8) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('8', 228 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 9) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('9', 317 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 10) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('10', 139 + 15, 94 + 44 * 6 + 30);
            if (PixelTanks.userData.level < 11) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('11', 228 + 15, 94 + 44 * 6 + 30);
            if (PixelTanks.userData.level < 12) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('12', 317 + 15, 94 + 44 * 6 + 30);
          },
        },
        levelSelect2: {
          buttons: [],
          exec: [],
          listeners: {},
          customOnLoad: () => {
            GUI.draw.fillStyle = '#556B2f';
            GUI.draw.fillRect(50, 50, 400, 400);
            GUI.draw.fillStyle = '#ffffff';
            if (PixelTanks.userData.level < 13) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94, 44, 44);
            if (PixelTanks.userData.level < 14) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94, 44, 44);
            if (PixelTanks.userData.level < 15) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94, 44, 44);
            if (PixelTanks.userData.level < 16) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 17) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 18) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 19) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 20) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 21) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 22) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 6, 44, 44);
            if (PixelTanks.userData.level < 23) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 6, 44, 44);
            if (PixelTanks.userData.level < 24) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 6, 44, 44);
            GUI.draw.fillStyle = '#000000';
            GUI.draw.font = '20px Neuropol';
            if (PixelTanks.userData.level < 13) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('13', 139 + 15, 94 + 30);
            if (PixelTanks.userData.level < 14) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('14', 228 + 15, 94 + 30);
            if (PixelTanks.userData.level < 15) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('15', 317 + 15, 94 + 30);
            if (PixelTanks.userData.level < 16) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('16', 139 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 17) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('17', 228 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 18) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('18', 317 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 19) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('19', 139 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 20) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('20', 228 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 21) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('21', 317 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 22) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('22', 139 + 15, 94 + 44 * 6 + 30);
            if (PixelTanks.userData.level < 23) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('23', 228 + 15, 94 + 44 * 6 + 30);
            if (PixelTanks.userData.level < 24) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('24', 317 + 15, 94 + 44 * 6 + 30);
          },
        },
        levelSelect3: {
          buttons: [],
          exec: [],
          listeners: [],
          customOnLoad: () => {
            GUI.draw.fillStyle = '#556B2f';
            GUI.draw.fillRect(50, 50, 400, 400);
            GUI.draw.fillStyle = '#ffffff';
            if (PixelTanks.userData.level < 25) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94, 44, 44);
            if (PixelTanks.userData.level < 26) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94, 44, 44);
            if (PixelTanks.userData.level < 27) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94, 44, 44);
            if (PixelTanks.userData.level < 28) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 29) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 30) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 31) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 32) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 33) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 34) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 6, 44, 44);
            if (PixelTanks.userData.level < 35) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 6, 44, 44);
            if (PixelTanks.userData.level < 36) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 6, 44, 44);
            GUI.draw.fillStyle = '#000000';
            GUI.draw.font = '20px Neuropol';
            if (PixelTanks.userData.level < 25) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('25', 139 + 15, 94 + 30);
            if (PixelTanks.userData.level < 26) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('26', 228 + 15, 94 + 30);
            if (PixelTanks.userData.level < 27) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('27', 317 + 15, 94 + 30);
            if (PixelTanks.userData.level < 28) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('28', 139 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 29) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('29', 228 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 30) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('30', 317 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 31) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('31', 139 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 32) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('32', 228 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 33) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('33', 317 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 34) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('34', 139 + 15, 94 + 44 * 6 + 30);
            if (PixelTanks.userData.level < 35) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('35', 228 + 15, 94 + 44 * 6 + 30);
            if (PixelTanks.userData.level < 36) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('36', 317 + 15, 94 + 44 * 6 + 30);
          }
        },
        levelSelect4: {
          buttons: [],
          exec: [],
          listeners: {},
          customOnLoad: () => {
            GUI.draw.fillStyle = '#556B2f';
            GUI.draw.fillRect(50, 50, 400, 400);
            GUI.draw.fillStyle = '#ffffff';
            if (PixelTanks.userData.level < 37) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94, 44, 44);
            if (PixelTanks.userData.level < 38) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94, 44, 44);
            if (PixelTanks.userData.level < 39) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94, 44, 44);
            if (PixelTanks.userData.level < 40) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 41) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 42) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 2, 44, 44);
            if (PixelTanks.userData.level < 43) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 44) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 45) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 4, 44, 44);
            if (PixelTanks.userData.level < 46) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(139, 94 + 44 * 6, 44, 44);
            if (PixelTanks.userData.level < 47) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(228, 94 + 44 * 6, 44, 44);
            if (PixelTanks.userData.level < 48) GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(317, 94 + 44 * 6, 44, 44);
            GUI.draw.fillStyle = '#000000';
            GUI.draw.font = '20px Neuropol';
            if (PixelTanks.userData.level < 37) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('37', 139 + 15, 94 + 30);
            if (PixelTanks.userData.level < 38) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('38', 228 + 15, 94 + 30);
            if (PixelTanks.userData.level < 39) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('39', 317 + 15, 94 + 30);
            if (PixelTanks.userData.level < 40) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('40', 139 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 41) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('41', 228 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 42) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('42', 317 + 15, 94 + 44 * 2 + 30);
            if (PixelTanks.userData.level < 43) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('43', 139 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 44) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('44', 228 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 45) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('45', 317 + 15, 94 + 44 * 4 + 30);
            if (PixelTanks.userData.level < 46) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('46', 139 + 15, 94 + 44 * 6 + 30);
            if (PixelTanks.userData.level < 47) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('47', 228 + 15, 94 + 44 * 6 + 30);
            if (PixelTanks.userData.level < 48) GUI.draw.fillStyle = '#A9A9A9';
            GUI.draw.fillText('48', 317 + 15, 94 + 44 * 6 + 30);
          }
        },
        pvp: {
          buttons: [{
            x: 59,
            y: 56,
            w: 53,
            h: 53, // square???
            ref: 'multiplayer',
          }, {
            x: 255,
            y: 389,
            w: 188,
            h: 55,
            ref: 'duels',
          }, {
            x: 48,
            y: 389,
            w: 188,
            h: 55,
            ref: 'ffa',
          }],
          exec: [],
          customOnLoad: () => {},
        },
        co_op: {
          buttons: [{

          }],
          exec: [],
          customOnLoad: () => {
            alert('This area is still in testing...');
            Menus.trigger('multiplayer');
          },
        },
        ffa: {
          buttons: [{
            x: 59,
            y: 56,
            w: 53,
            h: 53,
            ref: 'pvp',
          }],
          exec: [{
            x: 158,
            y: 233,
            w: 188,
            h: 55,
            ref: `(() => {
            Menus.removeListeners();
            PixelTanks.user.joiner = new MultiPlayerTank();
            PixelTanks.user.joiner.control();
            // join ffa server with ip and room (or quick join)
          })();`
          }],
          listeners: {},
          customOnLoad: () => {
            // initialize key input for server ip and join type
          },
        },
        duels: {
          buttons: [{
            x: 59,
            y: 56,
            w: 53,
            h: 53,
            ref: 'pvp',
          }],
          exec: [{

          }],
          listeners: {},
          customOnLoad: () => {},
        },
        crate: {
          buttons: [{
            x: 59,
            y: 56,
            w: 53,
            h: 53,
            ref: 'main',
          }, {
            x: 388,
            y: 57,
            w: 53,
            h: 53,
            ref: 'cosmetic' // my cosmetics area
          }],
          exec: [{
            x: 69,
            y: 303,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`red`);',
          }, {
            x: 323,
            y: 145,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`orange`);',
          }, {
            x: 323,
            y: 223,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`yellow`);',
          }, {
            x: 323,
            y: 306,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`green`);',
          }, {
            x: 198,
            y: 305,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`blue`);',
          }, {
            x: 197,
            y: 222,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`purple`);',
          }, {
            x: 69,
            y: 143,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`mark`);',
          }, {
            x: 323,
            y: 383,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`grey`);',
          }, {
            x: 68,
            y: 220,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`christmas`);',
          }, {
            x: 198,
            y: 382,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`halloween`);',
          }, {
            x: 69,
            y: 380,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`misc`);',
          }, {
            x: 198,
            y: 145,
            w: 53,
            h: 53,
            ref: 'PixelTanks.openCrate(`pizza`);',
          }],
          listeners: {},
          customOnLoad: () => {
            GUI.draw.textAlign = 'center';
            GUI.draw.fillText('Crates: ' + PixelTanks.userData.crates, 250, 130);
            GUI.draw.textAlign = 'left';
          }
        },
        cosmetic: {
          buttons: [{
            x: 59,
            y: 56,
            w: 53,
            h: 53,
            ref: 'crate',
          }],
          exec: [], // handled later
          listeners: {
            keydown: (e) => {
              if (e.keyCode === 37 && Menus.menus.cosmetic.menuNum != 0) {
                Menus.menus.cosmetic.menuNum--;
              } else if (e.keyCode === 39 && (Menus.menus.cosmetic.menuNum + 1) * 30 < PixelTanks.userData.cosmetics.length) {
                Menus.menus.cosmetic.menuNum++;
              }
              Menus.redraw();
            },
            mousedown: (e) => {
              var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - PixelTanks.offset * PixelTanks.resizer;
              var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
              x /= PixelTanks.resizer;
              y /= PixelTanks.resizer;
              var y2 = (y - ((y - 140) % 59)) / 59;
              var x2 = (x - ((x - 78) % 59)) / 59;
              var y3, x3;
              if (((y - 140) % 59 < y2 + 50) && y > 140) {
                y3 = Math.floor(y2 - 2);
              }
              if (((x - 78) % 59 < x2 + 50) && x > 78) {
                x3 = Math.floor(x2);
              }
              if (x3 != undefined && y3 != undefined) {
                if (e.button === 0) {
                  PixelTanks.userData.cosmetic = PixelTanks.userData.cosmetics[Menus.menus.cosmetic.menuNum * 30 + y3 * 6 + x3 - 1];
                } else {
                  PixelTanks.userData.cosmetics.splice(Menus.menus.cosmetic.menuNum * 30 + y3 * 6 + x3 - 1, 1);
                }
                Menus.redraw();
              }
            }
          },
          customOnLoad: () => {
            var cosmetic = Menus.menus.cosmetic;
            if (!cosmetic.menuNum) {
              cosmetic.menuNum = 0;
            }

            var totalStages = (PixelTanks.userData.cosmetics.length - (PixelTanks.userData.cosmetics.length % 30)) / 30;

            if (cosmetic.menuNum === 0) {
              GUI.draw.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].cosmeticLeft, 0, 0);
            } else if (cosmetic.menuNum !== totalStages) {
              GUI.draw.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].cosmeticCenter, 0, 0);
            } else {
              GUI.draw.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].cosmeticRight, 0, 0);
            }

            var l = cosmetic.menuNum * 30;
            while (l < Math.min(cosmetic.menuNum * 30 + 30, PixelTanks.userData.cosmetics.length)) {
              if (PixelTanks.userData.cosmetic === PixelTanks.userData.cosmetics[l]) {
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeStyle = '#FFA500';
                GUI.draw.strokeRect(((l - cosmetic.menuNum * 30) % 6) * 59 + 78 - 2, (((l - cosmetic.menuNum * 30) - ((l - cosmetic.menuNum * 30) % 6)) / 6) * 59 + 140 - 2, 50, 50);
              }
              var q = 0;
              while (q < PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
                if (PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[q].name === PixelTanks.userData.cosmetics[l]) {
                  GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[q].image, ((l - cosmetic.menuNum * 30) % 6) * 59 + 78, (((l - cosmetic.menuNum * 30) - ((l - cosmetic.menuNum * 30) % 6)) / 6) * 59 + 140);
                }
                q++;
              }
              l++;
            }
          }
        },
        settings: {
          buttons: [{
            x: 59,
            y: 56,
            w: 53,
            h: 53,
            ref: 'main',
          }, {
            x: 397,
            y: 65,
            w: 38,
            h: 35,
            ref: 'keybinds',
          }],
          exec: [],
          listeners: {},
          customOnLoad: () => {}
        },
        keybinds: {
          buttons: [{
            x: 59,
            y: 56,
            w: 53,
            h: 53,
            ref: 'settings',
          }],
          exec: [],
          listeners: {
            keydown: (e) => {
              if (Menus.menus.keybinds.keybind !== undefined) {
                PixelTanks.userData.settings[Menus.menus.keybinds.keybind] = e.keyCode;
              }
              Menus.redraw();
              PixelTanks.save();
            },
            mousedown: (e) => {
              var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - ((window.innerWidth-window.innerHeight*1.6)/2)*PixelTanks.resizer;
              var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
              x /= PixelTanks.resizer;
              y /= PixelTanks.resizer;
              if ((x > 70 && x < 150) && (y > 140 && y < 190)) {
                Menus.menus.keybinds.keybind = 'item1';
              } else if ((x > 70 && x < 150) && (y > 200 && y < 250)) {
                Menus.menus.keybinds.keybind = 'item2';
              } else if ((x > 70 && x < 150) && (y > 260 && y < 310)) {
                Menus.menus.keybinds.keybind = 'item3';
              } else if ((x > 70 && x < 150) && (y > 320 && y < 370)) {
                Menus.menus.keybinds.keybind = 'item4';
              } else if ((x > 70 && x < 150) && (y > 380 && y < 430)) {
                Menus.menus.keybinds.keybind = 'shoot';
              } else if ((x > 260 && x < 340) && (y > 140 && y < 190)) {
                Menus.menus.keybinds.keybind = 'fire1';
              } else if ((x > 260 && x < 340) && (y > 200 && y < 250)) {
                Menus.menus.keybinds.keybind = 'fire2';
              } else {
                Menus.menus.keybinds.keybind = undefined;
              }
              Menus.redraw();
            },
          },
          customOnLoad: () => {
            GUI.draw.strokeStyle = '#ffffff';
            GUI.draw.lineWidth = 5;
            switch (Menus.menus.keybinds.keybind) {
              case 'item1':
                GUI.draw.strokeRect(70, 140, 80, 50);
                break;
              case 'item2':
                GUI.draw.strokeRect(70, 200, 80, 50);
                break;
              case 'item3':
                GUI.draw.strokeRect(70, 260, 80, 50);
                break;
              case 'item4':
                GUI.draw.strokeRect(70, 320, 80, 50);
                break;
              case 'shoot':
                GUI.draw.strokeRect(70, 380, 80, 50);
                break;
              case 'fire1':
                GUI.draw.strokeRect(260, 140, 80, 50);
                break;
              case 'fire2':
                GUI.draw.strokeRect(260, 200, 80, 50);
                break;
            }
            GUI.draw.fillStyle = '#000000';
            GUI.draw.font = '20px Neuropol';
            GUI.draw.fillText(PixelTanks.userData.settings.item1, 190, 170);
            GUI.draw.fillText(PixelTanks.userData.settings.item2, 190, 230);
            GUI.draw.fillText(PixelTanks.userData.settings.item3, 190, 290);
            GUI.draw.fillText(PixelTanks.userData.settings.item4, 190, 350);
            GUI.draw.fillText(PixelTanks.userData.settings.shoot, 190, 410);
            GUI.draw.fillText(PixelTanks.userData.settings.fire1, 420, 150);
            GUI.draw.fillText(PixelTanks.userData.settings.item5, 420, 210);
          },
        },
        inventory: {
          buttons: [{
            x: 58,
            y: 55,
            w: 54,
            h: 55,
            ref: 'main',
          }, {
            x: 228,
            y: 110,
            w: 44,
            h: 44,
            ref: 'crate',
          }],
          exec: [{
            x: 382,
            y: 229,
            w: 43,
            h: 44,
            ref: `(() => {
            if (Menus.menus.inventory.healthTab === undefined) Menus.menus.inventory.healthTab = false;
            Menus.menus.inventory.healthTab = !Menus.menus.inventory.healthTab;
            Menus.menus.inventory.classTab = false;
            Menus.menus.inventory.itemTab = false;
            Menus.redraw();
          })();`,
          }, {
            x: 406,
            y: 407,
            w: 44,
            h: 44,
            ref: `(() => {
            if (Menus.menus.inventory.classTab === undefined) Menus.menus.inventory.classTab = false;
            Menus.menus.inventory.classTab = !Menus.menus.inventory.classTab;
            Menus.menus.inventory.healthTab = false;
            Menus.menus.inventory.itemTab = false;
            Menus.redraw();
          })();`
          }, {
            x: 50,
            y: 407,
            w: 44,
            h: 44,
            ref: `(() => {
            if (Menus.menus.inventory.itemTab === undefined) Menus.menus.inventory.itemTab = false;
            Menus.menus.inventory.itemTab = !Menus.menus.inventory.itemTab;
            Menus.menus.inventory.healthTab = false;
            Menus.menus.inventory.classTab = false;
            Menus.menus.inventory.currentItem = 1;
            Menus.redraw(); 
          })();`,
          }, {
            x: 94,
            y: 407,
            w: 44,
            h: 44,
            ref: `(() => {
            if (Menus.menus.inventory.itemTab === undefined) Menus.menus.inventory.itemTab = false;
            Menus.menus.inventory.itemTab = !Menus.menus.inventory.itemTab;
            Menus.menus.inventory.healthTab = false;
            Menus.menus.inventory.classTab = false;
            Menus.menus.inventory.currentItem = 2;
            Menus.redraw(); 
          })();`,
          }, {
            x: 138,
            y: 407,
            w: 44,
            h: 44,
            ref: `(() => {
            if (Menus.menus.inventory.itemTab === undefined) Menus.menus.inventory.itemTab = false;
            Menus.menus.inventory.itemTab = !Menus.menus.inventory.itemTab;
            Menus.menus.inventory.healthTab = false;
            Menus.menus.inventory.classTab = false;
            Menus.menus.inventory.currentItem = 3;
            Menus.redraw(); 
          })();`,
          }, {
            x: 182,
            y: 407,
            w: 44,
            h: 44,
            ref: `(() => {
            if (Menus.menus.inventory.itemTab === undefined) Menus.menus.inventory.itemTab = false;
            Menus.menus.inventory.itemTab = !Menus.menus.inventory.itemTab;
            Menus.menus.inventory.healthTab = false;
            Menus.menus.inventory.classTab = false;
            Menus.menus.inventory.currentItem = 4;
            Menus.redraw(); 
          })();`,
          }],
          listeners: {
            mousedown: (e) => {
              var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - ((window.innerWidth-window.innerHeight*1.6)/2)*PixelTanks.resizer;
              var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
              x /= PixelTanks.resizer;
              y /= PixelTanks.resizer;
              if (Menus.menus.inventory.healthTab) {
                if (x > 227 && x < 272 && y > 120 && y < 164) {
                  PixelTanks.userData.material = 0;
                }
                if (x > 227 && x < 272 && y > 174 && y < 218) {
                  if (PixelTanks.userData.steel) {
                    PixelTanks.userData.material = 1;
                  }
                }
                if (x > 227 && x < 272 && y > 228 && y < 272) {
                  if (PixelTanks.userData.crystal) {
                    PixelTanks.userData.material = 2;
                  }
                }
                if (x > 227 && x < 272 && y > 282 && y < 326) {
                  if (PixelTanks.userData.dark) {
                    PixelTanks.userData.material = 3;
                  }
                }
                if (x > 227 && x < 272 && y > 336 && y < 380) {
                  if (PixelTanks.userData.light) {
                    PixelTanks.userData.material = 4;
                  }
                }
              }
              if (Menus.menus.inventory.classTab) {
                // top: 7 left: 7, width: 44, height: 44 id: tactical
                //194=offsetx
                //140=offsety
                //201=button-left-column-top-left-corner-x
                //201+44
                if (x > 201 && x < 245 && y > 147 && y < 191) {
                  if (PixelTanks.userData.tactical) {
                    PixelTanks.userData.class = 'tactical';
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 258 && x < 299 && y > 147 && y < 191) {
                  if (PixelTanks.userData.stealth) {
                    PixelTanks.userData.class = 'stealth';
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 258 && x < 299 && y > 201 && y < 245) {
                  if (PixelTanks.userData.builder) {
                    PixelTanks.userData.class = 'builder';
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 258 && x < 299 && y > 255 && y < 299) {
                  if (PixelTanks.userData.warrior) {
                    PixelTanks.userData.class = 'warrior';
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 258 && x < 299 && y > 309 && y < 353) {
                  if (PixelTanks.userData.medic) {
                    PixelTanks.userData.class = 'medic';
                  } else {
                    alert('You need to buy this first');
                  }
                }
              }
              if (Menus.menus.inventory.itemTab) {
                var key = [{
                  x: 150,
                  y: 150,
                  n: 'airstrike',
                }, {
                  x: 204,
                  y: 150,
                  n: 'super_glu', 
                }, {
                  x: 258,
                  y: 150,
                  n: 'duck_tape',
                }, {
                  x: 312,
                  y: 150,
                  n: 'shield',
                }, {
                  x: 150,
                  y: 204,
                  n: 'flashbang',
                }, {
                  x: 204,
                  y: 204,
                  n: 'bomb',
                }, {
                  x: 258,
                  y: 204,
                  n: 'dynamite',
                }, {
                  x: 312,
                  y: 204,
                  n: 'power_bomb',
                }, {
                  x: 150,
                  y: 258,
                  n: 'weak',
                }, {
                  x: 204,
                  y: 258,
                  n: 'strong',
                }, {
                  x: 258,
                  y: 258,
                  n: 'spike',
                }, {
                  x: 302,
                  y: 258,
                  n: 'mine',
                }, {
                  x: 150,
                  y: 302,
                  n: 'fortress',
                }];
                var l = 0;
                while (l<key.length) {
                  if (x > key[l].x && x < key[l].x+40 && y > key[l].y && y < key[l].y+40) {
                    var q = 1, s = 0;
                    while (q<5) {
                      if (PixelTanks.userData['item'+q] === key[l].n) {
                        s++;
                      }
                      q++;
                    }
                    if (s >= 2) {
                      alert('No more than 2 of an item idot');
                    } else {
                      PixelTanks.userData['item'+Menus.menus.inventory.currentItem] = key[l].n;
                    }
                  }
                  l++;
                }
              }
              Menus.redraw();
              PixelTanks.save();
            },
            mousemove: (e) => {
              var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - ((window.innerWidth-window.innerHeight*1.6)/2)*PixelTanks.resizer;
              var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
              x /= PixelTanks.resizer;
              y /= PixelTanks.resizer;
              Menus.menus.inventory.target = {
                x: x - 800,
                y: y - 500,
              }
              Menus.redraw();
            },
            keydown: (e) => {
              if (e.key.length === 1) {
                Menus.menus.inventory.color += e.key;
              } else if (e.keyCode === 8) {
                Menus.menus.inventory.color = Menus.menus.inventory.color.slice(0, -1);
              }
              PixelTanks.userData.color = Menus.menus.inventory.color;
              Menus.redraw();
            }
          },
          customOnLoad: () => {
            var key = {
              0: 'red',
              1: 'iron',
              2: 'diamond',
              3: 'dark',
              4: 'light',
            };

            if (PixelTanks.userData.color === undefined) PixelTanks.userData.color = '#A9A9A9';
            if (Menus.menus.inventory.color === undefined) Menus.menus.inventory.color = PixelTanks.userData.color;
            GUI.draw.fillStyle = PixelTanks.userData.color;
            GUI.draw.fillRect(354, 132, 20, 20);
            GUI.draw.fillStyle = '#000000';
            GUI.draw.font = '20px Neuropol';
            GUI.draw.textAlign = 'left';
            GUI.draw.fillText(Menus.menus.inventory.color, 376, 128);

            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[PixelTanks.userData.material]].top, 382, 229, 43, 44);
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[PixelTanks.userData.material]].bottom[0], 190, 190, 120, 120);
            // class draw here
            GUI.draw.drawImage(PixelTanks.images.items[PixelTanks.userData.item1], 50, 407, 40, 40);
            GUI.draw.drawImage(PixelTanks.images.items[PixelTanks.userData.item2], 94, 407, 40, 40);
            GUI.draw.drawImage(PixelTanks.images.items[PixelTanks.userData.item3], 138, 407, 40, 40);
            GUI.draw.drawImage(PixelTanks.images.items[PixelTanks.userData.item4], 182, 407, 40, 40);

            if (Menus.menus.inventory.target === undefined) Menus.menus.inventory.target = {
              x: 0,
              y: 0
            };

            var angle = Math.atan2(Menus.menus.inventory.target.x, Menus.menus.inventory.target.y) * 180 / Math.PI
            angle = -angle;
            if (angle < 0) {
              angle += 360;
            }
            if (angle >= 360) {
              angle -= 360;
            }

            GUI.draw.translate(250, 250);
            GUI.draw.rotate(angle * Math.PI / 180);
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[PixelTanks.userData.material]].top, -60, -60, 120, 135);
            if (PixelTanks.userData.cosmetic != '' || PixelTanks.userData.cosmetic != undefined) {
              var l = 0;
              while (l < PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
                if (PixelTanks.userData.cosmetic == PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].name) {
                  GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].image, -60, -60, 120, 135);
                  break; // CLEANUP is this faster?????
                }
                l++;
              }
            }
            GUI.draw.rotate(-angle * Math.PI / 180);
            GUI.draw.translate(-250, -250);
            if (Menus.menus.inventory.healthTab) {
              GUI.draw.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].healthTab, 221, 113);
              if (PixelTanks.userData.material === 0) {
                GUI.draw.strokeStyle = '#FFFF00';
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeRect(227, 120, 44, 44);
              }
              if (PixelTanks.userData.material === 1) {
                GUI.draw.strokeStyle = '#FFFF00';
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeRect(227, 174, 44, 44);
              }
              if (PixelTanks.userData.material === 2) {
                GUI.draw.strokeStyle = '#FFFF00';
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeRect(227, 228, 44, 44);
              }
              if (PixelTanks.userData.material === 3) {
                GUI.draw.strokeStyle = '#FFFF00';
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeRect(227, 282, 44, 44);
              }
              if (PixelTanks.userData.material === 4) {
                GUI.draw.strokeStyle = '#FFFF00';
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeRect(227, 336, 44, 44);
              }
            }
            if (Menus.menus.inventory.classTab) {
              GUI.draw.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].classTab, 194, 140);
              if (PixelTanks.userData.class === 'tactical') {
                GUI.draw.strokeStyle = '#FFFF00';
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeRect(201, 147, 44, 44);
              }
              if (PixelTanks.userData.class === 'stealth') {
                GUI.draw.strokeStyle = '#FFFF00';
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeRect(258, 147, 44, 44);
              }
              if (PixelTanks.userData.class === 'builder') {
                GUI.draw.strokeStyle = '#FFFF00';
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeRect(258, 201, 44, 44);
              }
              if (PixelTanks.userData.class === 'warrior') {
                GUI.draw.strokeStyle = '#FFFF00';
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeRect(258, 255, 44, 44);
              }
              if (PixelTanks.userData.class === 'medic') {
                GUI.draw.strokeStyle = '#FFFF00';
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeRect(258, 309, 44, 44);
              }
            }
            if (Menus.menus.inventory.itemTab) {
              GUI.draw.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].itemTab, 140, 140);
              GUI.draw.drawImage(PixelTanks.images.items.airstrike, 150, 150, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.super_glu, 204, 150, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.duck_tape, 258, 150, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.shield, 312, 150, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.flashbang, 150, 204, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.bomb, 204, 204, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.dynamite, 258, 204, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.power_bomb, 312, 204, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.weak, 150, 258, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.strong, 204, 258, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.spike, 258, 258, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.mine, 302, 258, 40, 40);
              GUI.draw.drawImage(PixelTanks.images.items.fortress, 150, 312, 40, 40);
            }
          },
        },
        itemshop: {
          buttons: [{
            x: 59,
            y: 56,
            w: 53,
            h: 53,
            ref: 'main',
          }, {
            x: 57,
            y: 145,
            w: 188,
            h: 55,
            ref: 'itemequip',
          }, {
            x: 254,
            y: 145,
            w: 188,
            h: 55,
            ref: 'itemequip',
          }, {
            x: 57,
            y: 205,
            w: 188,
            h: 55,
            ref: 'itemequip',
          }, {
            x: 254,
            y: 205,
            w: 188,
            h: 55,
            ref: 'itemequip',
          }, {
            x: 82,
            y: 319,
            w: 344,
            h: 99,
            ref: 'itembuy',
          }],
          exec: [],
          listeners: {},
          customOnLoad: () => {},
        },
        shop: {
          buttons: [{
            x: 59,
            y: 56,
            w: 53,
            h: 53, // square???
            ref: 'main',
          }, {
            x: 255,
            y: 389,
            w: 188,
            h: 55,
            ref: 'classshop',
          }, {
            x: 57,
            y: 220,
            w: 188,
            h: 55,
            ref: 'main',
          }, {
            x: 48,
            y: 389,
            w: 188,
            h: 55,
            ref: 'main',
          }, {
            x: 254,
            y: 220,
            w: 188,
            h: 55,
            ref: 'armorshop',
          }],
          exec: [],
          listeners: {},
          customOnLoad: () => {},
        },
        armorshop: {
          buttons: [{
            x: 59,
            y: 56,
            w: 53,
            h: 53, // square???
            ref: 'shop'
          }],
          exec: [{
            x: 57,
            y: 220,
            w: 188,
            h: 55,
            ref: `(function() {
              var stat = 'steel';
              var price = 10000;
              PixelTanks.purchase(stat, price, 0);
            })();`,
          }, {
            x: 254,
            y: 220,
            w: 188,
            h: 55,
            ref: `(function() {
              var stat = 'crystal';
              var price = 20000;
              PixelTanks.purchase(stat, price, 0);
            })();`, //crystal
          }, {
            x: 48,
            y: 389,
            w: 188,
            h: 55,
            ref: `(function() {
              var stat = 'dark';
              var price = 30000;
              PixelTanks.purchase(stat, price, 0);
            })();`,
          }, {
            x: 255,
            y: 389,
            w: 188,
            h: 55,
            ref: `(function() {
              var stat = 'light';
              var price = 40000;
              PixelTanks.purchase(stat, price, 0);
            })();`,
          }],
          listeners: {},
          customOnLoad: () => {
            GUI.draw.textAlign = 'center';
            GUI.draw.fillText(' ' + PixelTanks.userData.key, 250, 135);
            GUI.draw.textAlign = 'left';
          },
        },
        itembuy: {
          buttons: [{
            x: 61,
            y: 61,
            w: 90,
            h: 30,
            ref: 'shop',
          }],
          exec: [],
          listeners: {
            mousedown: (e) => {
              var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - ((window.innerWidth-window.innerHeight*1.6)/2)*PixelTanks.resizer;
              var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
              x /= PixelTanks.resizer;
              y /= PixelTanks.resizer;
              var items = [{
                x: 60,
                y: 144,
                w: 91,
                h: 50,
                i: 'toolkit',
                p: 1000,
              }, {
                x: 60,
                y: 199,
                w: 91,
                h: 50,
                i: 'duct_tape',
                p: 750,
              }, {
                x: 60,
                y: 254,
                w: 91,
                h: 50,
                i: 'super_glu',
                p: 1000,
              }, {
                x: 60,
                y: 311,
                w: 91,
                h: 50,
                i: 'shield',
                p: 500,
              }, {
                x: 160,
                y: 144,
                w: 91,
                h: 50,
                i: 'flashbang',
                p: 1500,
              }, {
                x: 160,
                y: 199,
                w: 91,
                h: 50,
                i: 'bomb',
                p: 750,
              }, {
                x: 160,
                y: 254,
                w: 91,
                h: 50,
                i: 'dynamite',
                p: 1500,
              }, {
                x: 160,
                y: 311,
                w: 91,
                h: 50,
                i: 'power-bomb',
                p: 2000,
              }, {
                x: 259,
                y: 144,
                w: 91,
                h: 50,
                i: 'weak',
                p: 250,
              }, {
                x: 259,
                y: 199,
                w: 91,
                h: 50,
                i: 'strong',
                p: 500,
              }, {
                x: 259,
                y: 254,
                w: 91,
                h: 50,
                i: 'spike',
                p: 1000,
              }, {
                x: 259,
                y: 311,
                w: 91,
                h: 50,
                i: 'landmine',
                p: 2000,
              }, {
                x: 353,
                y: 144,
                w: 91,
                h: 50,
                i: 'grapple',
                p: 750,
              }, {
                x: 353,
                y: 199,
                w: 91,
                h: 50,
                i: 'hookshot',
                p: 1000,
              }, {
                x: 353,
                y: 254,
                w: 91,
                h: 50,
                i: 'grabble',
                p: 1000,
              }, {
                x: 353,
                y: 311,
                w: 91,
                h: 50,
                i: 'stunhook',
                p: 5000,
              }];
              var l = 0,
                available = ['toolkit', 'flashbang', 'bomb', 'power_bomb', 'duck_tape'];
              while (l < items.length) {
                if (x > items[l].x && x < items[l].x + items[l].w) {
                  if (y > items[l].y && y < items[l].y + items[l].h) {
                    if (PixelTanks.userData.coins >= items[l].p) {
                      if (available.includes(items[l].i)) {
                        PixelTanks.userData.coins -= items[l].p;
                        PixelTanks.userData[items[l].i + 's']++;
                      }
                    }
                  }
                }
                l++;
              }
            }
          },
          customOnLoad: () => {
            GUI.draw.textAlign = 'center';
            GUI.draw.fillStyle = '#000000';
            GUI.draw.fillText(' ' + PixelTanks.userData.toolkits, 105, 184);
            GUI.draw.fillText(' ' + PixelTanks.userData.flashbangs, 206, 184);
            GUI.draw.fillText(' ' + PixelTanks.userData.blocks, 304, 184);
            GUI.draw.fillStyle = '#FFFFFF';
            GUI.draw.textAlign = 'left';
          },
        },
        classshop: {
          buttons: [{
            x: 61,
            y: 61,
            w: 90,
            h: 30, // square???
            ref: 'shop'
          }],
          exec: [{
            x: 95,
            y: 184,
            w: 73,
            h: 78, // square???
            ref: `(function() {
              var stat = 'tactical';
              var price = 30000;
              PixelTanks.purchase(stat, price, 0);
            })();`,
          }, {
            x: 175,
            y: 184,
            w: 73,
            h: 78, // square???
            ref: `(function() {
              var stat = 'stealth';
              var price = 30000;
              PixelTanks.purchase(stat, price, 0);
            })();`,
          }, {
            x: 255,
            y: 184,
            w: 73,
            h: 78, // square???
            ref: `(function() {
              var stat = 'builder';
              var price = 30000;
              PixelTanks.purchase(stat, price, 0);
            })();`,
          }, {
            x: 335,
            y: 184,
            w: 73,
            h: 78, // square???
            ref: 'alert("This class is not added yet :(")',
          }, {
            x: 95,
            y: 268,
            w: 73,
            h: 78, // square???
            ref: `(function() {
              var stat = 'medic';
              var price = 30000;
              PixelTanks.purchase(stat, price, 0);
            })();`,
          }, {
            x: 175,
            y: 268,
            w: 73,
            h: 78, // square???
            ref: `(function() {
              var stat = 'warrior';
              var price = 30000;
              PixelTanks.purchase(stat, price, 0);
            })();`,
          }, {
            x: 255,
            y: 268,
            w: 73,
            h: 78, // square???
            ref: 'alert("This class is not added yet :(")',
          }, {
            x: 335,
            y: 268,
            w: 73,
            h: 78, // square???
            ref: 'alert("This class is not added yet :(")',
          }],
          listeners: {},
          customOnLoad: () => {
            GUI.draw.textAlign = 'center';
            GUI.draw.fillStyle = '#000000';
            GUI.draw.fillText(' ' + PixelTanks.userData.class, 250, 390);
            GUI.draw.textAlign = '#FFFFFF';
            GUI.draw.textAlign = 'left';
          },
        },
        defeat: '/images/menus/default/defeat.png',
        victory: '/images/menus/default/victory.png',
      }

      // Add Images to Asset loader
      var toLoad = [];
      for (var property in PixelTanks.images.blocks) {
        for (var blok in PixelTanks.images.blocks[property]) {
          toLoad.push('blocks:'+property+':'+blok);
          PIXI.Assets.add('blocks:'+property+':'+blok, PixelTanks.images.blocks[property][blok]);
        }
      }
      
      for (var property in PixelTanks.images.bullets) {
        for (var bullet in PixelTanks.images.bullets[property]) {
          toLoad.push('bullets:'+property+':'+bullet);
          PIXI.Assets.add('bullets:'+property+':'+bullet, PixelTanks.images.bullets[property][bullet]);
        }
      }

      for (var property in PixelTanks.images.tanks) {
        for (var tank in PixelTanks.images.tanks[property]) {
          if (tank != 'cosmetics' && tank != 'other') {
            toLoad.push('tanks:'+property+':'+tank+':top');
            PIXI.Assets.add('tanks:'+property+':'+tank+':top', PixelTanks.images.tanks[property][tank].top);
            toLoad.push('tanks:'+property+':'+tank+':bottom:'+0);
            PIXI.Assets.add('tanks:'+property+':'+tank+':bottom:'+0, PixelTanks.images.tanks[property][tank].bottom[0]);
            toLoad.push('tanks:'+property+':'+tank+':bottom:'+1);
            PIXI.Assets.add('tanks:'+property+':'+tank+':bottom:'+1, PixelTanks.images.tanks[property][tank].bottom[1]);
          }
        }
        var l = 0;
        while (l < PixelTanks.images.tanks[property].cosmetics.length) {
          PixelTanks.images.tanks[property].cosmetics[l].image = '';
          toLoad.push('tanks:'+property+':cosmetics:'+l+':image');
          PIXI.Assets.add('tanks:'+property+':cosmetics:'+l+':image', '/images/tanks/'+property+'/cosmetics/'+PixelTanks.images.tanks[property].cosmetics[l].id+'.png');
          l++;
        }
        for (var item in PixelTanks.images.tanks[property].other) {
          toLoad.push('tanks:'+property+':'+item);
          PIXI.Assets.add('tanks:'+property+':'+item, PixelTanks.images.tanks[property].other[item]);
        }
      }

      for (var property in PixelTanks.images.menus) {
        for (var menu in PixelTanks.images.menus[property]) {
          toLoad.push('menus:'+property+':'+menu);
          PIXI.Assets.add('menus:'+property+':'+menu, PixelTanks.images.menus[property][menu]);
        }
      }

      for (var property in PixelTanks.images.emotes) {
        toLoad.push('emotes:'+property);
        PIXI.Assets.add('emotes:'+property, PixelTanks.images.emotes[property].image);
      }

      for (var property in PixelTanks.images.animations) {
        toLoad.push('animations:'+property);
        PIXI.Assets.add('animations:'+property, PixelTanks.images.animations[property].image);
      }

      for (var property in PixelTanks.images.items) {
        toLoad.push('items:'+property);
        PIXI.Assets.add('items:'+property, PixelTanks.images.items[property]);
      }

      // load menus

      for (var property in Menus.menus) {
        if (property.includes('levelSelect')) {
          var menuNum = property.replace('levelSelect', '');
          if (menuNum == '') menuNum = 1;
          menuNum = new Number(menuNum);
          var y = 0;
          while (y < 4) {
            var x = 0;
            while (x < 3) {
              menuActions[property].exec.push({
                x: 89 * x + 140,
                y: 88 * y + 94,
                w: 44,
                h: 44,
                ref: `(() => { PixelTanks.user.world = new World(); PixelTanks.user.world.level = ` + (12 * (menuNum - 1) + (x + y * 3) + 1) + `; Menus.removeListeners(); PixelTanks.user.world.init();})();`,
              });
              x++;
            }
            y++;
          }
        }
        Menus.menus[property] = new Menu(function() { // No arrow function here
          if (PixelTanks.images.menus[PixelTanks.texturepack][this.id]) {
            GUI.drawImage(PixelTanks.images.menus[PixelTanks.texturepack][this.id], 0, 0, 1600, 1000, 1);
          }
          this.data.customOnLoad = this.data.customOnLoad.bind(this);
          this.data.customOnLoad();
        }, {
          click: Menus.onclick,
          ...menuActions[property].listeners,
        }, window);
        Menus.menus[property].data = menuActions[property];
        Menus.menus[property].id = property;
      }

      PixelTanks.socket = new MegaSocket('wss://' + window.location.hostname, {
        keepAlive: false,
      });

      // Load Assets
      var assets = await PIXI.Assets.load(toLoad, this.updateBootProgress);
      for (var property in assets) {
        var l = 0, ref = '';
        while (l<property.split(':').length) {
          ref += '["'+property.split(':')[l]+'"]';
          l++;
        }
        eval('PixelTanks.images'+ref+' = assets["'+property+'"];');
      }

      setTimeout(PixelTanks.launch, 200);
    }

    static launch() {
      Menus.trigger('start');
    }

    static save() {
      try {
        var temp = PixelTanks.playerData;
        temp['pixel-tanks'] = PixelTanks.userData;
        Network.update('playerdata', JSON.stringify(temp));
      } catch (e) {
        // PixelTanks.user is not logged in
        console.log('User Not Logged In?: ERR->' + e);
      }
    }

    static openCrate(crate) {

      if (PixelTanks.userData.crates > 0) {
        PixelTanks.userData.crates--;
      } else {
        alert('You need to get crates to open them. You can get crates by beating players in multiplayer');
        return;
      }

      var rarity;
      if (Math.floor(Math.random() * (1000 - 0 + 1)) < 1) {
        rarity = 'mythic';
      } else if (Math.floor(Math.random() * (1001)) < 10) {
        rarity = 'legendary';
      } else if (Math.floor(Math.random() * (1001)) < 50) {
        rarity = 'epic';
      } else if (Math.floor(Math.random() * (1001)) < 150) {
        rarity = 'rare';
      } else if (Math.floor(Math.random() * (1000 - 0 + 1)) < 300) {
        rarity = 'uncommon';
      } else {
        rarity = 'common';
      }
      var items = [];
      var l = 0;
      while (l < PixelTanks.crates[crate].length) {
        if (PixelTanks.crates[crate][l].rarity == rarity) {
          items.push(PixelTanks.crates[crate][l].name);
        }
        l++;
      }
      var number = Math.floor(Math.random() * (items.length));
      var c;
      var l = 0;
      while (l < PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
        if (PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].name == items[number]) {
          c = PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l];
        }
        l++;
      }
      GUI.draw.clearRect(0, 0, 500, 500);
      GUI.draw.drawImage(c.image, 150, 200, 200, 200);
      GUI.draw.textAlign = 'center';
      GUI.draw.fillStyle = '#ffffff';
      GUI.draw.fillText(c.name, 250, 400);
      GUI.draw.textAlign = 'left';
      setTimeout(() => {
        Menus.redraw();
      }, 2000);
      if (PixelTanks.userData.cosmetics.includes(c.name)) {
        setTimeout(function() {
          var t = confirm('You already have this. Sell for 100G?');
          if (t) {
            PixelTanks.userData.coins += 100;
          } else {
            PixelTanks.userData.cosmetics.push(c.name);
            PixelTanks.save();
          }
        }, 2000);
        return;
      }
      PixelTanks.userData.cosmetics.push(c.name);
      PixelTanks.save();
    }

    static purchase(stat, price, buyType) {
      if (PixelTanks.userData[stat] === true) {
        alert('You already bought this :/');
      } else if (PixelTanks.userData.coins >= price) {
        PixelTanks.userData.coins -= price;
        if (buyType === 0) { // change from false to true
          PixelTanks.userData[stat] = true;
        } else if (buyType === 1) {
          PixelTanks.userData[stat]++;
        }
        alert(`
        Purchased Processed
        Price: `+price+`
        Item: PixelTanks.userData.`+stat+`
        VALUE: `+PixelTanks.userData[stat]+`
        `)
      } else {
        alert('Your too broke to buy this');
      }
    }
  }

  class Ai {
    constructor(type, fireType, isInvis, turretFireType, team) {
      if (type == 'turret') {
        this.turret = true;
      } else {
        this.turret = false;
      }
      this.generatePath = this.generatePath.bind(this);
      this.speed = 2;
      this.oldTarget = {
        x: null,
        y: null,
      }
      this.team = team;
      this.fireType = fireType;
      this.sawUser = false;
      this.instructions = {};
      this.type = type;
      this.rotation = 0;
      this.base = 0;
      this.pushback = 0;
      this.fire = this.fire.bind(this);
      if (!this.turret) {
        if (fireType == 0) {
          this.health = 60;
        } else if (fireType == 1) {
          this.health = 200;
        } else if (fireType == 2) {
          this.health = 320;
        } else if (fireType == 3) {
          this.health = 250;
        }
      } else { // Turrets have x3 health
        if (fireType == 1) {
          this.health = 200;
        }
      }
      this.maxHealth = this.health;
      this.inactive = false;
      this.update = true;
      this.leftright = false;
      this.canShoot = true;
      this.canTakeThermalDamage = true;
    }
    draw() {
      if (this.inactive != true) {
        if (this.leftright) {
          if (!this.turret) {
            GUI.draw.translate(this.x + 20 + PixelTanks.offset, this.y + 20);
          } else {
            GUI.draw.translate(this.x + 25 + PixelTanks.offset, this.y + 25);
          }
          GUI.draw.rotate(90 * Math.PI / 180);
          if (this.base == 1) {
            if (!this.turret) {
              if (this.fireType == 0) {
                GUI.draw.drawImage(stupid_tank_base, -20, -20)
              } else if (this.fireType == 1) {
                GUI.draw.drawImage(tank_base_png, -20, -20);
              } else if (this.fireType == 2) {
                GUI.draw.drawImage(area_tank_base, -20, -20);
              } else if (this.fireType == 3) {
                GUI.draw.drawImage(condensed_tank_base, -20, -20);
              }
            } else {
              GUI.draw.drawImage(ai_turret_base, -25, -25, 50, 50);
            }
          } else {
            if (!this.turret) {
              if (this.fireType == 0) {
                GUI.draw.drawImage(stupid_tank_base2, -20, -20);
              } else if (this.fireType == 1) {
                GUI.draw.drawImage(tank_base2, -20, -20);
              } else if (this.fireType == 2) {
                GUI.draw.drawImage(area_tank_base2, -20, -20);
              } else if (this.fireType == 3) {
                GUI.draw.drawImage(condensed_tank_base2, -20, -20);
              }
            } else {
              GUI.draw.drawImage(ai_turret_base, -25, -25, 50, 50);
            }
          }
          GUI.draw.rotate(-90 * Math.PI / 180);
          if (!this.turret) {
            GUI.draw.translate((-this.x - 20 - PixelTanks.offset), (-this.y - 20));
          } else {
            GUI.draw.translate((-this.x - 25 - PixelTanks.offset), (-this.y - 25));
          }
        } else {
          if (this.base == 1) {
            if (!this.turret) {
              if (this.fireType == 0) {
                GUI.draw.drawImage(stupid_tank_base, this.x, this.y);
              } else if (this.fireType == 1) {
                GUI.draw.drawImage(tank_base_png, this.x, this.y);
              } else if (this.fireType == 2) {
                GUI.draw.drawImage(area_tank_base, this.x, this.y);
              } else if (this.fireType == 3) {
                GUI.draw.drawImage(condensed_tank_base, this.x, this.y);
              }
            } else {
              GUI.draw.drawImage(ai_turret_base, this.x, this.y, 50, 50);
            }
          } else {
            if (!this.turret) {
              if (this.fireType == 0) {
                GUI.draw.drawImage(stupid_tank_base2, this.x, this.y);
              } else if (this.fireType == 1) {
                GUI.draw.drawImage(tank_base2, this.x, this.y);
              } else if (this.fireType == 2) {
                GUI.draw.drawImage(area_tank_base2, this.x, this.y);
              } else if (this.fireType == 3) {
                GUI.draw.drawImage(condensed_tank_base2, this.x, this.y);
              }
            } else {
              GUI.draw.drawImage(ai_turret_base, this.x, this.y, 50, 50);
            }
          }
        }
        if (!this.turret) {
          GUI.draw.translate(this.x + 20 + PixelTanks.offset, this.y + 20);
        } else {
          GUI.draw.translate(this.x + 25 + PixelTanks.offset, this.y + 25);
        }
        GUI.draw.rotate(this.rotation * Math.PI / 180);
        if (!this.turret) {
          if (this.fireType == 0) {
            GUI.draw.drawImage(stupid_tank_top, -20, -20 + this.pushback);
          } else if (this.fireType == 1) {
            GUI.draw.drawImage(ai_top, -20, -20 + this.pushback);;
          } else if (this.fireType == 2) {
            GUI.draw.drawImage(area_tank_top, -20, -20 + this.pushback);
          } else if (this.fireType == 3) {
            GUI.draw.drawImage(condensed_tank_top, -20, -20 + this.pushback);
          }
        } else {
          if (this.fireType == 1) {
            GUI.draw.drawImage(ai_top, -20, -20 + this.pushback);
          }
        }
        if (this.pushback != 0) {
          this.pushback += 1;
        }
        GUI.draw.rotate(-(this.rotation * Math.PI / 180));
        if (!this.turret) {
          GUI.draw.translate(-this.x - 20 - PixelTanks.offset, -this.y - 20);
        } else {
          GUI.draw.translate(-this.x - 25 - PixelTanks.offset, -this.y - 25);
        }
        GUI.draw.fillStyle = '#000000';
        GUI.draw.fillRect(this.x, this.y + 50, 40, 5);
        GUI.draw.fillStyle = '#FF0000';
        GUI.draw.fillRect(this.x + 2, this.y + 51, 36 * this.health / this.maxHealth, 3);
      }
    }
    sight() {}
    spotIdiot() {
      // right now only the player is an option
      var possibleTargets = [];
      // If player is Celestial or this tank is a summoned tank, change the target spotting to the reverse :)
      possibleTargets.push({
        x: PixelTanks.user.tank.x,
        y: PixelTanks.user.tank.y,
      });
      // add more targets here >:D (like spefically Celestial);
      var l = 0;
      while (l < possibleTargets.length) {
        var distance = Math.sqrt(Math.pow(this.x - possibleTargets[l].x, 2) + Math.pow(this.y - possibleTargets[l].y, 2));
        possibleTargets[l].distance = distance;
        l++;
      }
      possibleTargets.sort(function(a, b) {
        return a.distance - b.distance;
      });
      this.target = possibleTargets[0];
    }
    move() {
      if (this.grappled) {
        if (this.x > this.grapplePos.x) {
          if (this.x - this.grapplePos.x < 20) {
            if (checker(this.x - (this.x - this.grapplePos.x), this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x -= this.x - this.grapplePos.x;
            } else {
              //this.grappled = false;
            }
          } else {
            if (checker(this.x - 20, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x -= 20;
            } else {
              //this.grappled = false;
            }
          }
        } else if (this.x < this.grapplePos.x) {
          if (this.grapplePos.x - this.x < 20) {
            if (checker(this.x + (this.grapplePos.x - this.x), this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x += this.grapplePos.x - this.x;
            } else {
              //this.grappled = false;
            }
          } else {
            if (checker(this.x + 20, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x += 20;
            } else {
              //this.grappled = false;
            }
          }
        }
        if (this.y > this.grapplePos.y) {
          if (this.y - this.grapplePos.y < 20) {
            if (checker(this.x, this.y - (this.y - this.grapplePos.y), {
                x: this.x,
                y: this.y
              })) {
              this.y -= this.y - this.grapplePos.y;
            } else {
              //this.grappled = false;
            }
          } else {
            if (checker(this.x, this.y - 20, {
                x: this.x,
                y: this.y
              })) {
              this.y -= 20;
            } else {
              //this.grappled = false;
            }
          }
        } else if (this.y < this.grapplePos.y) {
          if (this.grapplePos.y - this.y < 20) {
            if (checker(this.x, this.y + (this.grapplePos.y - this.y), {
                x: this.x,
                y: this.y
              })) {
              this.y += this.grapplePos.y - this.y;
            } else {
              //this.grappled = false;
            }
          } else {
            if (checker(this.x, this.y + 20, {
                x: this.x,
                y: this.y
              })) {
              this.y += 20;
            } else {
              //this.grappled = false;
            }
          }
        }
        if (this.x == this.grapplePos.x && this.y == this.grapplePos.y) {
          this.grappled = false;
        }
        setTimeout(function() { // BIND
          if (this.grappled) this.grappled = false;
        }.bind(this), 10000);
        return;
      }
      if (!this.inactive) {
        this.spotIdiot(); // Find Target
        if ((Math.floor(this.oldTarget.x / 50) == Math.floor(this.target.x / 50)) && (Math.floor(this.oldTarget.y / 50) == Math.floor(this.target.y / 50))) {
          this.instructions.path = this.oldPath;
          this.instructions.movement = this.oldMovement;
        } else {
          this.generatePath();
        }
        if (this.instructions.movement && this.instructions.path.length != 0) { // check if movement is a valid value
          var pos = [];
          pos[0] = {
            x: this.x / 50,
            y: this.y / 50,
          }
          pos[1] = {
            x: (this.x + 40) / 50,
            y: this.y / 50,
          }
          pos[2] = {
            x: this.x / 50,
            y: (this.y + 40) / 50,
          }
          pos[3] = {
            x: (this.x + 40) / 50,
            y: (this.y + 40) / 50,
          }
          var l = 0,
            result = [],
            outcome = {
              x: 0,
              y: 0
            };
          while (l < pos.length) {
            result[l] = false;
            var q = 0;
            while (q < this.instructions.path.length) {
              if (Math.floor(pos[l].x) == this.instructions.path[q][0]) {
                if (Math.floor(pos[l].y) == this.instructions.path[q][1]) {
                  result[l] = true;
                }
              }
              q++;
            }
            l++;
          }
          var remap = false;
          if ((!result[0] || !result[1] || !result[2] || !result[3]) && !(result[1] && result[2] && !result[0] && !result[3]) && !(result[0] && !result[1] && !result[2] && result[3])) {
            remap = true;
            if (!result[0] && !result[1]) {
              outcome.y += this.speed;
            }
            if (!result[0] && !result[2]) {
              outcome.x += this.speed;
            }
            if (!result[1] && !result[3]) {
              outcome.x -= this.speed;
            }
            if (!result[2] && !result[3]) {
              outcome.y -= this.speed;
            }
            if (checker(this.x + outcome.x, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x += outcome.x;
            }
            if (checker(this.x, this.y + outcome.y, {
                x: this.x,
                y: this.y
              })) {
              this.y += outcome.y;
            }
            return;
          }
          if (this.instructions.movement[0] == 2) {
            if (checker(this.x, this.y - this.speed, {
                x: this.x,
                y: this.y
              })) {
              this.y -= this.speed;
            }
          }
          if (this.instructions.movement[0] == 1) {
            if (checker(this.x, this.y + this.speed, {
                x: this.x,
                y: this.y
              })) {
              this.y += this.speed;
            }
          }
          if (this.instructions.movement[0] == 3) {
            if (checker(this.x - this.speed, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x -= this.speed;
            }
          }
          if (this.instructions.movement[0] == 4) {
            if (checker(this.x + this.speed, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x += this.speed;
            }
          }
          if (this.instructions.movement[0] == this.speed) {
            if (checker(this.x - this.speed, this.y - this.speed, {
                x: this.x,
                y: this.y
              })) {
              this.x -= this.speed;
              this.y -= this.speed;
            } else if (checker(this.x - this.speed, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x -= this.speed;
            } else if (checker(this.x, this.y - this.speed, {
                x: this.x,
                y: this.y
              })) {
              this.y -= this.speed;
            }
          }
          if (this.instructions.movement[0] == 6) {
            if (checker(this.x + this.speed, this.y - this.speed, {
                x: this.x,
                y: this.y
              })) {
              this.x += this.speed;
              this.y -= this.speed;
            } else if (checker(this.x + this.speed, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x += this.speed;
            } else if (checker(this.x, this.y - this.speed, {
                x: this.x,
                y: this.y
              })) {
              this.y -= this.speed;
            }
          }
          if (this.instructions.movement[0] == 7) {
            if (checker(this.x - this.speed, this.y + this.speed, {
                x: this.x,
                y: this.y
              })) {
              this.x -= this.speed;
              this.y += this.speed;
            } else if (checker(this.x - this.speed, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x -= this.speed;
            } else if (checker(this.x, this.y + this.speed, {
                x: this.x,
                y: this.y
              })) {
              this.y += this.speed;
            }
          }
          if (this.instructions.movement[0] == 8) {
            if (checker(this.x + this.speed, this.y + this.speed, {
                x: this.x,
                y: this.y
              })) {
              this.x += this.speed;
              this.y += this.speed;
            } else if (checker(this.x + this.speed, this.y, {
                x: this.x,
                y: this.y
              })) {
              this.x += this.speed;
            } else if (checker(this.x, this.y + this.speed, {
                x: this.x,
                y: this.y
              })) {
              this.y += this.speed;
            }
          }
        }
        this.oldTarget = this.target;
        this.oldPath = this.instructions.path;
        this.oldMovement = this.instructions.movement;
      }
    }
    generatePath() {
      var map = [];
      var l = 0;
      while (l < PixelTanks.user.world.map.length) {
        var section = PixelTanks.user.world.map[l].split('');
        var q = 0;
        while (q < section.length) {
          section[q] = new Number(section[q]);
          q++;
        }
        map[l] = section;
        l++;
      }
      var grid = new PF.Grid({...map});
      var finder = new PF.AStarFinder({
        allowDiagonal: true,
        dontCrossCorners: true,
      });
      var path = finder.findPath(Math.floor((this.x + 20) / 50), Math.floor((this.y + 20) / 50), Math.floor(this.target.x / 50), Math.floor(this.target.y / 50), grid);
      var l = 0,
        directions = [],
        currentPos = {
          x: Math.floor((this.x + 20) / 50),
          y: Math.floor((this.y + 20) / 50),
        };
      // 1 -> GO DOWN -y
      // 2 -> GO UP +y
      // 3 -> GO LEFT -x
      // 4 -> GO RIGHT +x
      // 5 -> GO UP/LEFT -x -y
      // 6 -> GO UP/RIGHT +x -y
      // 7 -> GO DOWN/LEFT +y -x
      // 8 -> GO DOWN/RIGHT +y +x
      while (l < path.length) {
        var d = {
          x: path[l][0] - currentPos.x,
          y: path[l][1] - currentPos.y,
        }
        if (d.x != 0 && d.x != 1 && d.x != -1) {
          alert('( ' + d.x + ', ' + d.y + ')');
        }
        if (d.x == 0 && d.y == 1) {
          directions.push(1);
          currentPos.y += 1;
        }
        if (d.x == 0 && d.y == -1) {
          directions.push(2);
          currentPos.y -= 1;
        }
        if (d.x == -1 && d.y == 0) {
          directions.push(3);
          currentPos.x -= 1;
        }
        if (d.x == 1 && d.y == 0) {
          directions.push(4);
          currentPos.x += 1;
        }
        if (d.x == -1 && d.y == -1) {
          directions.push(5);
          currentPos.y -= 1;
          currentPos.x -= 1;
        }
        if (d.x == 1 && d.y == -1) {
          directions.push(6);
          currentPos.x += 1;
          currentPos.y -= 1;
        }
        if (d.x == -1 && d.y == 1) {
          directions.push(7);
          currentPos.x -= 1;
          currentPos.y += 1;
        }
        if (d.x == 1 && d.y == 1) {
          directions.push(8);
          currentPos.x += 1;
          currentPos.y += 1;
        }
        l++;
      }
      this.instructions = {
        movement: directions,
        path: path,
      };
    }
    shoot(instance) {
      if (!instance.inactive) {
        if (instance.seeUser) {
          instance.fire();
        }
      }
    }
    fire() {
      this.pushback = -3;
      var xd = (this.x + 20) - (PixelTanks.user.tank.x + 20);
      var yd = (this.y + 20) - (PixelTanks.user.tank.y + 20);
      if (xd < 0) {
        var neg = false;
      } else {
        var neg = true;
      }
      if (xd == 0) {
        xd = 1;
      }
      yd = yd / xd;
      xd = 1;
      if (neg) {
        xd = 0 - 1;
        yd = 0 - yd;
      }
      var resize = 0;
      if (this.turret) resize = 5;
      if (this.fireType == 1) {
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd, yd, 'bullet', this.team, this.rotation, 0));
      } else if (this.fireType == 2) {
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd, yd, 'shotgun_bullet', this.team, this.rotation, 0));
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd + xd * .6, yd, 'shotgun_bullet', this.team, this.rotation, 0));
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd, yd - yd * .6, 'shotgun_bullet', this.team, this.rotation, 0));
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd, yd + yd * .9, 'shotgun_bullet', this.team, this.rotation, 0));
        s.push(new Shot(this.x + 20 + resize, this.y + 20 + resize, s.length - 1, xd - xd * .9, yd, 'shotgun_bullet', this.team, this.rotation, 0));
      }
    }
    check() {
      if (PixelTanks.userData.kit == 'thermal') {
        if (thermal_check(this.x, this.y) && this.canTakeThermalDamage) {
          this.health -= 20;
          if (this.health <= 0) {
            var max, min;
            if (this.fireType == 0) {
              if (Math.random() < .1 || PixelTanks.userData.cosmetic == 'MLG Glasses') {
                PixelTanks.user.world.xp += 1;
              }
              max = 50;
              min = 0;
            } else if (this.fireType == 1) {
              if (Math.random() < .1 || PixelTanks.userData.cosmetic == 'MLG Glasses') {
                PixelTanks.user.world.xp += 2;
              }
              max = 200;
              min = 100;
            } else if (this.fireType == 2) {
              if (Math.random() < .1 || PixelTanks.userData.cosmetic == 'MLG Glasses') {
                PixelTanks.user.world.xp += 5;
              }
              max = 500;
              min = 200;
            } else if (this.fireType == 3) {
              if (Math.random() < .1) {
                PixelTanks.user.world.xp += 5;
              }
              max = 500;
              min = 200;
            }
            if (PixelTanks.userData.kit == 'scavenger') {
              PixelTanks.user.world.coins += 3 * (Math.floor(Math.random() * max) + min);
            } else {
              PixelTanks.user.world.coins += Math.floor(Math.random() * max) + min;
            }
            PixelTanks.user.world.foes--;
            this.x = undefined;
            this.y = undefined;
            this.inactive = true;
            if (PixelTanks.user.world.foes == 0) {
              document.exitFullscreen();
              if (PixelTanks.user.world.level < 10000) {
                setTimeout(victory, 200);
                PixelTanks.user.world.endGame();
              }
            }
          }
          this.canTakeThermalDamage = false;
          setTimeout(function(tank) {
            tank.canTakeThermalDamage = true;
          }, 1000, this);
        }
      }
    }
    damage(s) {
      if (s.type == 'grapple') {
        this.grappled = true;
        this.grapplePos = {
          startx: this.x,
          starty: this.y,
          x: results[3].startx - 20,
          y: results[3].starty - 20,
        }
      }
      GUI.draw.fillStyle = '#FF0000';
      GUI.draw.fillRect(this.x, this.y, 40, 40);
      this.health -= s.damage;
      this.sawUser = true; // detects PixelTanks.user if shot
      if (this.health <= 0) {
        var max, min;
        if (this.fireType == 0) {
          if (Math.random() < .1) {
            PixelTanks.user.world.xp += 1;
          }
          max = 50;
          min = 0;
        } else if (this.fireType == 1) {
          if (Math.random() < .1) {
            PixelTanks.user.world.xp += 2;
          }
          max = 200;
          min = 100;
        } else if (this.fireType == 2) {
          if (Math.random() < .1) {
            PixelTanks.user.world.xp += 5;
          }
          max = 500;
          min = 200;
        } else if (this.fireType == 3) {
          if (Math.random() < .1) {
            PixelTanks.user.world.xp += 5;
          }
          max = 500;
          min = 200;
        }
        if (PixelTanks.userData.kit == 'scavenger') {
          PixelTanks.user.world.coins += 2 * (Math.floor(Math.random() * max) + min);
        } else {
          PixelTanks.user.world.coins += Math.floor(Math.random() * max) + min;
        }
        PixelTanks.user.world.foes--;
        this.x = undefined;
        this.y = undefined;
        this.inactive = true;
        if (PixelTanks.user.world.foes == 0) {
          document.exitFullscreen();
          if (PixelTanks.user.world.level < 10000) {
            setTimeout(victory, 200);
            PixelTanks.user.world.endGame();
          }
        }
      }
    }
  }

class Shot {
  constructor(x, y, xm, ym, t, r, u, h) {
    const settings = {
      damage: {
        bullet: 20,
        shotgun: 20,
        grapple: 0,
        powermissle: 100,
        megamissle: 200,
      },
      speed: {
        bullet: .5,
        shotgun: .8,
        grapple: 2,
        powermissle: 1.5,
        megamissle: 1.5,
      }
    }

    this.d = 0;
    this.u = u;
    this.r = r;
    this.t = t;
    this.sx = x;
    this.sy = y;
    this.host = h;

    var d = 5;
    this.xm = xm*(d/Math.sqrt(xm*xm+ym*ym));
    this.ym = ym*(d/Math.sqrt(xm*xm+ym*ym));
    var data = Shot.calc(x, y, xm, ym);
    this.x = data.x;
    this.y = data.y;

    if (xm == 0 && ym == 1) {
      this.x = x;
      this.y = 35+y;
    } else if (xm == -1 && ym == 0) {
      this.x = -35+x;
      this.y = y;
    } else if (xm == 0 && ym == -1) {
      this.x = x;
      this.y = -35+y;
    } else if (xm == 1 && ym == 0) {
      this.x = 35+x;
      this.y = y;
    }

    this.damage = settings.damage[this.t];
    /*var l = 0;
    while (l<this.host.pt.length) {
      if (this.host.pt[l].u === this.u && this.host.pt[l].buff) {
        this.damage *= 1.3;
      }
      l++;
    }*/
    this.xm *= settings.speed[this.t];
    this.ym *= settings.speed[this.t];
  }

  static calc(x, y, xm, ym) {
    var sgn, x, y;
    if (((ym/xm)*-20-(ym/xm)*20)<0) {
      sgn = -1;
    } else {
      sgn = 1;
    }
    var x1 = ((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)+sgn*-40*Math.sqrt(1000*(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)))-((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*(20*(ym/xm)*-20-(-20)*(ym/xm)*20))))/(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)));
    var x2 = ((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)-sgn*-40*Math.sqrt(1000*(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)))-((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*(20*(ym/xm)*-20-(-20)*(ym/xm)*20))))/(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)));
    var y1 = (-(20*(ym/xm)*-20-(-20)*(ym/xm)*20)*-40+ Math.abs(((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(1000*(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)))-((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*(20*(ym/xm)*-20-(-20)*(ym/xm)*20))))/(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)));
    var y2 = (-(20*(ym/xm)*-20-(-20)*(ym/xm)*20)*-40-Math.abs(((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(1000*(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))) - ((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*(20*(ym/xm)*-20-(-20)*(ym/xm)*20))))/(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)));
    if (ym >= 0) {
      x = x1+x;
      y = y1+y;
    } else {
      x = x2+x;
      y = y2+y;
    }
    return {
      x: x,
      y: y,
    };
  }

  collision() {
    var x = this.x;
    var y = this.y;
    // collision with player here

    var l = 0;
    while (l<this.host.ai.length) {
      if ((x > this.host.ai[l].x || x + 5 > this.host.ai[l].x) && (x < this.host.ai[l].x + 40 || x + 5 < this.host.ai[l].x + 40)) {
        if ((y > this.host.ai[l].y || y + 5 > this.host.ai[l].y) && (y < this.host.ai[l].y + 40 || y + 5 < this.host.ai[l].y + 40)) {
          if (this.u !== this.host.ai[l].o) {
            this.host.ai[l].damage(this);
            return [true, this.u];
          } else {
            return [true, this.u];
          }
          break;
        }
      }
      l++;
    }
    
    var l = 0;
    while (l < this.host.b.length) {
      if ((x > this.host.b[l].x || x+5 > this.host.b[l].x) && (x < this.host.b[l].x+50 || x+5 < this.host.b[l].x+50)) {
        if ((y > this.host.b[l].y || y+5 > this.host.b[l].y) && (y < this.host.b[l].y+50 || y+5 < this.host.b[l].y+50)) {
          if (this.host.b[l].c) {
            if (this.t === 'grapple') {
              var q = 0;
              while (q<this.host.pt.length) {
                if (this.host.pt[q].u === this.u) {
                  if (this.host.pt[q].grapple) {
                    this.host.pt[q].grapple.bullet.destroy();
                  }
                  this.host.pt[q].grapple = {
                    sx: this.host.pt[q].x,
                    sy: this.host.pt[q].y,
                    x: this.x,
                    y: this.y,
                    bullet: this,
                  }
                }
                q++;
              }
              this.update = () => {};
            } else {
              this.host.b[l].damage(this);
              return [true, this.u];
            }
            break;
          }
        }
      }
      l++;
    }

    if (x < 0 || x > 1500 || y < 0 || y > 1500) {
      return [true, null];
    }
    return [false, null];
  }

  update() {
    this.x += this.xm;
    this.y += this.ym;
    if (this.collision()[0]) {
      this.destroy();
      return;
    }

    this.d += Math.sqrt(Math.pow(this.xm, 2)+Math.pow(this.ym, 2));

    if (this.t == 'shotgun') {
      this.damage = 20-(this.d/150)*20;
      if (this.d >= 150) {
        this.destroy();
      }
    }
  }

  draw(t) {
      var q = 0;
      while (q < t.length) {
        if (this.u === t[q].u) {
          GUI.draw.fillStyle = t[q].col;
          GUI.draw.fillRect(this.x - 1, this.y - 1, 7, 7);
        }
        q++;
      }

      GUI.draw.translate(this.x + 2.5, this.y + 2.5);
      GUI.draw.rotate((this.r + 180) * Math.PI / 180);
      if (this.t == 'bullet') {
        GUI.draw.fillStyle = '#000000';
        GUI.draw.fillRect(-2.5, -2.5, 5, 5);
      } else if (this.t == 'powermissle') {
        GUI.draw.drawImage(PixelTanks.images.bullets[PixelTanks.texturepack].powermissle, -5, -10, 10, 20);
      } else if (this.t == 'megamissle') {
        GUI.draw.drawImage(PixelTanks.images.bullets[PixelTanks.texturepack].megamissle, -5, -10, 10, 20);
      } else if (this.t == 'shotgun') {
        GUI.draw.drawImage(PixelTanks.images.bullets[PixelTanks.texturepack].shotgun, -2.5, -2.5);
      } else if (this.t == 'grapple') {
        GUI.draw.drawImage(PixelTanks.images.bullets[PixelTanks.texturepack].grapple, -22.5, -22.5);
      }
      GUI.draw.rotate(-(this.r + 180) * Math.PI / 180);
      GUI.draw.translate(-this.x - 2.5, -this.y - 2.5);
    }

  destroy() {
    this.host.s.splice(this.host.s.indexOf(this), 1);
  }
}

  class MultiPlayerTank {
    control() {
      this.SETTINGS = {
        fps: false, // chromebooks cant handle this :( 
      }

      this.timers = {
        boost: new Date('Nov 28 2006'),
        powermissle: new Date('Nov 28 2006'),
        toolkit: new Date('Nov 28 2006'),
        class: {
          date: new Date('Nov 28 2006'),
          cooldown: -1,
        },
        items: [{
          date: new Date('Nov 28 2006'),
          cooldown: -1,
        }, {
          date: new Date('Nov 28 2006'),
          cooldown: -1,
        }, {
          date: new Date('Nov 28 2006'),
          cooldown: -1,
        }, {
          date: new Date('Nov 28 2006'),
          cooldown: -1,
        }],
      };

      this.xp = 0;
      this.crates = 0;
      this.kills = 0;
      this.hostupdate = {};
      this.fireType = 1;

      this.halfSpeed = false;
      this.canFire = true;
      this.canBoost = true;
      this.canToolkit = true;
      this.canPowermissle = true;
      this.canMegamissle = true;
      this.canInvis = true;
      this.canTurret = true;
      this.canBuild = true;
      this.canBuff = true;
      this.canHeal = true;
      this.canDynamite = true;
      this.hasDynamite = false;
      this.canItem0 = true;
      this.canItem1 = true;
      this.canItem2 = true;
      this.canItem3 = true;

      this.gamemode = 'ffa';
      this.canChangePaused = true;
      this.paused = false;
      this.speed = 2;
      this.helper = [];
      this.intervals = []; // optimize rename to i
      this.intervals2 = [];
      this.left = null;
      this.up = null;
      this.flatting = false;
      this.grapples = 1;
      this.canGrapple = true;
      this.showChat = false;
      this.msg = '';

      this.tank = {
        t: PixelTanks.userDatakit === 'thermal', // thermal [J]
        s: 0, // shields [U]
        x: 0, // x [U, J]
        y: 0, // y [U, J]
        r: 0, // rotation [U, J]
        h: PixelTanks.userData.health, // max health [J]
        e: null, // emote [U]
        ra: PixelTanks.userData.rank, // rank [j]
        br: true, // base rotation [J, U]
        u: PixelTanks.user.username, // username [J]
        to: false, // toolkit [U]
        b: false, // place block [U]
        ba: false, // base first -> false, second -> true
        p: 0, // pushback [U, J]
        pl: 0, // place scaffolding [U]
        fl: false, // flashbang fired [U]
        i: false, // immune [U]
        in: false, // invis  [U]
        c: PixelTanks.userData.class, // class [J]
        co: PixelTanks.userData.cosmetic, // cosmetic [J]
        fi: [], // firing
        a: false, // animations
        mi: false, // place mine
      };

      this.tank.m = PixelTanks.userData.material;

      this.socket = new MegaSocket('wss://' + window.location.hostname + '/ffa-server', {
        keepAlive: false,
        reconnect: false,
        autoconnect: true,
      });

      this.socket.on('message', function(data) {
        this.ups++;
        if (this.paused) return;
        if (data.event == 'hostupdate') {
          this.timestamp = new Date().getTime()-data.timestamp;
          if (data.tanks) {
            this.hostupdate.tanks = data.tanks;
          }
          if (data.ai) {
            this.hostupdate.ai = data.ai;
          }
          if (data.blocks) {
            this.hostupdate.blocks = data.blocks;
          }
          if (data.bullets) {
            this.hostupdate.bullets = data.bullets;
          }
          if (data.explosions) {
            this.hostupdate.explosions = data.explosions;
          }
          if (data.logs) {
            this.hostupdate.logs = data.logs;
          }
        } else if (data.event == 'ded') {
          this.halfSpeed = false;
          this.canFire = true;
          this.canBoost = true;
          this.canToolkit = true;
          this.canPowermissle = true;
          this.canMegamissle = true;
          this.canInvis = true;
          this.canTurret = true;
          this.canBuild = true;
          this.canBuff = true;
          this.canHeal = true;
          this.canDynamite = true;
          this.hasDynamite = false;
          this.canItem0 = true;
          this.canItem1 = true;
          this.canItem2 = true;
          this.canItem3 = true;
          this.kills = 0;
          this.timers = {
            boost: new Date('Nov 28 2006'),
            powermissle: new Date('Nov 28 2006'),
            toolkit: new Date('Nov 28 2006'),
            class: {
              date: new Date('Nov 28 2006'),
              cooldown: -1,
            },
            items: [{
              date: new Date('Nov 28 2006'),
            cooldown: -1,
            }, {
              date: new Date('Nov 28 2006'),
              cooldown: -1,
            }, {
              date: new Date('Nov 28 2006'),
              cooldown: -1,
            }, {
              date: new Date('Nov 28 2006'),
            cooldown: -1,
            }],
          };
        } else if (data.event == 'gameover') {
          gameover(data.data);
        } else if (data.event == 'override') {
          var l = 0;
          while (l < data.data.length) {
            this.tank[data.data[l].key] = data.data[l].value;
            l++;
          }
        } else if (data.event == 'kill') {
          this.kills++;
          var crates = Math.floor(Math.random() * (2) + 1);
          this.xp += 10;
          this.crates += crates;
          PixelTanks.userData.crates += crates;
          PixelTanks.userData.xp += 10;
          PixelTanks.save();
        } else if (data.event == 'ping') {
          this.ping = new Date().getTime() - this.pingStart;
        }
      }.bind(this));

      this.socket.on('connect', function() {
        this.socket.send({
          username: PixelTanks.user.username,
          type: 'join',
          tank: {
            t: PixelTanks.userDatakit === 'thermal', // thermal [J]
            x: 0, // x [U, J]
            y: 0, // y [U, J]
            r: 0, // rotation [U, J]
            p: 0, // pushback [U, J]
            h: (PixelTanks.userData.material * 50 + 300), // max health [J]
            ra: PixelTanks.userData.rank, // rank [J]
            br: 0, // base rotation [J]
            u: PixelTanks.user.username, // username [J]
            ba: 0, // base image stage [U, J]
            c: PixelTanks.userData.class, // class [J]
            co: PixelTanks.userData.cosmetic, // cosmetic [J]
            m: this.tank.m,
            col: PixelTanks.userData.color,
          },
        });

        this.pingStart = new Date().getTime();
        this.pingId = Math.random();
        this.socket.send({
          type: 'ping',
          id: this.pingId,
        })

        if (!this.SETTINGS.fps) {
          setInterval(this.send.bind(this), 1000/60);
          requestAnimationFrame(this.frame.bind(this));
        }
      }.bind(this));

      document.addEventListener('keydown', this.keydown.bind(this));
      document.addEventListener('keyup', this.keyup.bind(this));
      document.addEventListener('mousemove', this.mousemove.bind(this));
      document.addEventListener('mousedown', this.mousedown.bind(this));
      document.addEventListener('mouseup', this.mouseup.bind(this));

      setInterval(function() {
        this.pingId = Math.random();
        this.pingStart = new Date().getTime();
        this.socket.send({
          type: 'ping',
          id: this.pingId,
        });
        // use stats here
        this.ops = 0;
        this.ups = 0;
        this.fps = 0;
      }.bind(this), 1000);
    }

    drawBlock(b) {
      GUI.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack][b.t], b.x*2, b.y*2, b.t === 'airstrike' ? 200 : 100, b.t === 'airstrike' ? 200 : 100, 1);
      if (b.s) {
        GUI.draw.beginFill(0x000000);
        GUI.draw.drawRect(b.x*2, b.y*2+110, 100, 5);
        GUI.draw.endFill();
        GUI.draw.beginFill(0x0000FF);
        GUI.draw.drawRect(b.x*2, b.y*2+110, 100*b.h/b.m, 5);
        GUI.draw.endFill();
      }
    }

    drawShot(s) {
      if (s.t == 'bullet') {
        GUI.draw.beginFill(0x000000);
        GUI.draw.drawRect(s.x*2, s.y*2, 10, 10)//-2.5, -2.5, 5, 5);
        GUI.draw.endFill();
      } else if (s.t === 'powermissle' || s.t === 'healmissle') {
        GUI.drawImageRotate(PixelTanks.images.bullets[PixelTanks.texturepack].powermissle, s.x*2, s.y*2, 20, 40, 5, 10, s.r+180, 1);
      } else if (s.t == 'megamissle') {
        GUI.drawImageRotate(PixelTanks.images.bullets[PixelTanks.texturepack].megamissle, s.x*2, s.y*2, 20, 40, 5, 10, s.r+180, 1); //-5, -10, 10, 20);
      } else if (s.t == 'shotgun') {
        GUI.drawImageRotate(PixelTanks.images.bullets[PixelTanks.texturepack].shotgun, s.x*2, s.y*2, 10, 10, 2.5, 2.5, s.r+180, 1); //-2.5, -2.5);
      } else if (s.t == 'grapple') {
        GUI.drawImageRotate(PixelTanks.images.bullets[PixelTanks.texturepack].grapple, s.x*2, s.y*2, 90, 90, 22.5, 22.5, s.r+180, 1); //-22.5, -22.5);
        /*GUI.draw.strokeStyle = 'darkgray';
        GUI.draw.rotate(-(s.r+180)*Math.PI/180);
        GUI.draw.lineWidth = 5;
        GUI.draw.beginPath();
        GUI.draw.moveTo(-2.5, -2.5);
        GUI.draw.lineTo(s.sx-s.x-2.5, s.sy-s.y-2.5);
        GUI.draw.stroke();
        GUI.draw.rotate((s.r+180)*Math.PI/180);*/
      } else if (s.t === 'dynamite') {
        GUI.drawImageRotate(PixelTanks.images.bullets[PixelTanks.texturepack].dynamite, s.x*2, s.y*2, 10, 40, 2.5, 2.5, s.r+180, 1);// -2.5, -10, 5, 20);
      }
    }

    drawExplosion(e) {
      //GUI.drawImage(PixelTanks.images.bullets[PixelTanks.texturepack].explosion, e.f*50, 0, 50, 50, e.x, e.y, e.w, e.h);
    }

    drawAi(a) {
      GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack]['red'].base, a.x, a.y, 40, 40);
      GUI.draw.translate(a.x + 20, a.y + 20);
      GUI.draw.rotate(a.r * Math.PI / 180);
      GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack]['red'].top, -20, -20+a.p);
      var l = 0;
      while (l<PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
        if (PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].name === a.c) {
          GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].image, -20, -20+a.p);
        }
        l++;
      }
      GUI.draw.rotate(-(a.r * Math.PI / 180));
      GUI.draw.translate(-a.x - 20, -a.y - 20);
      GUI.draw.fillStyle = '#ffffff';
      GUI.draw.fillStyle = '#000000';
      GUI.draw.fillRect(a.x, a.y + 50, 40, 5);
      GUI.draw.fillStyle = '#00FF00';
      GUI.draw.fillRect(a.x+2, a.y+51, 36*a.hp/600, 3);
    }

    drawTank(t) {
      var key = {
        0: 'red',
        1: 'iron',
        2: 'diamond',
        3: 'dark',
        4: 'light',
      };
      var a = 1;
      if (t.ded || t.in) a = .5;
      GUI.drawImageRotate(PixelTanks.images.tanks[PixelTanks.texturepack][key[t.m]].bottom[t.ba ? 0 : 1], t.x*2+40, t.y*2+40, 80, 80, 20, 20, t.br, a);
      GUI.drawImageRotate(PixelTanks.images.tanks[PixelTanks.texturepack][key[t.m]].top, t.x*2+40, t.y*2+40, 80, 90, 20, 20-t.p, t.r, a);

      if (t.co) {
        var q = 0;
        while (q < PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
          if (PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[q].name === t.co) {
            GUI.drawImageRotate(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[q].image, t.x*2+40, t.y*2+40, 80, 90, 20, 20-t.p, t.r, a);
          }
          q++;
        }
      }

      if (!t.ded) { // health bar
        GUI.draw.beginFill(0x000000);
        GUI.draw.drawRect(t.x*2, t.y*2+100, 80, 5);
        GUI.draw.endFill();
        GUI.draw.beginFill(0x90EE90);
        GUI.draw.drawRect(t.x*2, t.y*2+100, 80*t.h/t.ma, 5);
        GUI.draw.endFill();
      }

      var username = '['+t.ra+'] '+t.u;
      if (t.t.split(':')[1].includes('@leader')) {
        username += ' ['+t.t.split(':')[1].replace('@leader', '')+'] (Leader)'
      } else if (t.t.split(':')[1].includes('@requestor#')) {
        username += ' [Requesting...] ('+t.t.split(':')[1].split('@requestor#')[1]+')';
      } else if (new Number(t.t.split(':')[1]) < 1) {} else {
        username += ' ['+t.t.split(':')[1]+']';
      }

      GUI.drawText(username, t.x*2+40, t.y*2-25, 30, t.col, 0.5);

      if (t.s > 0 && !t.in) {
        GUI.draw.beginFill(0x7DF9FF);
        GUI.draw.alpha = .2;
        GUI.draw.drawCircle(t.x*2+40, t.y*2+40, 66);
        GUI.draw.endFill();
        GUI.draw.alpha = 1;
      }

      if (t.buff) {
        GUI.drawImage(PixelTanks.images.tanks.default.other.buff, t.x*2-5, t.y*2-5, 80, 80, .2);
      }

      var q = 0;
      if (t.d !== false) {
        if (PixelTanks.user.username === t.u) {
          GUI.drawText((Math.round(t.d.d) < 0) ? '+' : '-', t.d.x, t.d.y, Math.round(t.d.d/10)+10, '#FFFFFF', 0.5);
          GUI.drawText((Math.round(t.d.d) < 0) ? '+' : '-', t.d.x, t.d.y, Math.round(t.d.d/10)+8, '#FF0000', 0.5);
        } else {
          GUI.drawText((Math.round(t.d.d) < 0) ? '+' : '-', t.d.x, t.d.y, Math.round(t.d.d/10)+10, '#FFFFFF', 0.5);
          GUI.drawText((Math.round(t.d.d) < 0) ? '+' : '-', t.d.x, t.d.y, Math.round(t.d.d/10)+8, '#00FF00', 0.5);
        }
      }
      return; // no support for animations yet :(
      if (t.e) {
        GUI.draw.drawImage(PixelTanks.images.emotes.speech.image, t.x + 45, t.y - 15);
        GUI.draw.drawImage(PixelTanks.images.emotes[t.e.a].image, t.e.f * 50, 0, 50, 50, t.x + 45, t.y - 15, 50, 50);
      }

      if (t.a) {
        GUI.draw.drawImage(PixelTanks.images.animations[t.a.i].image, t.a.f * 40, 0, 40, 45, t.x, t.y, 40, 45);
      }
    }

    frame() {
      requestAnimationFrame(this.frame.bind(this));
      GUI.clear();
      GUI.resetSpritePool();
      GUI.resetTextPool();
      this.fps++;
      var t = this.hostupdate.tanks, b = this.hostupdate.blocks, s = this.hostupdate.bullets, a = this.hostupdate.ai, e = this.hostupdate.explosions;
      this.stats = '(' + this.ops + ', ' + this.ups + ', ' + this.fps + ') ping: ' + this.ping + ' ['+this.hostupdate.bullets.length+', '+this.hostupdate.blocks.length+', '+this.hostupdate.tanks.length+'] timestamp: '+this.timestamp;

      var l = 0;
      while (l<t.length) {
        if (t[l].u == PixelTanks.user.username) {
          t[l].x = this.tank.x;
          t[l].y = this.tank.y;
          t[l].r = this.tank.r;
          t[l].br = this.tank.br; // player smoothing
          GUI.root.pivot.set(t[l].x*2-760, t[l].y*2-460);
          GUI.draw.pivot.set(t[l].x*2-760, t[l].y*2-460);
        }
        l++;
      }

      GUI.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack].floor, 0, 0, 3000, 3000, 1);

      var l = 0;
      while (l<b.length) {
        this.drawBlock(b[l]);
        l++;
      }

      var l = 0;
      while (l<s.length) {
        this.drawShot(s[l]);
        l++;
      }

      var l = 0;
      while (l<a.length) {
        //this.drawAi(PixelTanks.user.joiner.hostupdate.ai[l]);
        l++;
      }

      var l = 0;
      while (l<t.length) {
        this.drawTank(t[l]);
        l++;
      }

      var l = 0;
      while (l<e.length) {
        this.drawExplosion(e[l]);
        l++;
      }

      var l = 0;
      while (l<t.length) {
        if (t[l].u == PixelTanks.user.username) {
          if (t[l].fb) {
            //GUI.draw.fillStyle = '#ffffff';
            //GUI.draw.drawImage(PixelTanks.images.menus.default.banged, 0, 0, canvas.width, canvas.height);
          }
        }
        l++;
      }

      GUI.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].ui, GUI.root.pivot.x, GUI.root.pivot.y, 1600, 1000, 1);

      /*GUI.drawImage(PixelTanks.images.items[PixelTanks.userData.item1], GUI.root.pivot.x, GUI.root.pivot.y, 100, 450);
      GUI.draw.drawImage(PixelTanks.images.items[PixelTanks.userData.item2], 183, 450);
      GUI.draw.drawImage(PixelTanks.images.items[PixelTanks.userData.item3], 266, 450);
      GUI.draw.drawImage(PixelTanks.images.items[PixelTanks.userData.item4], 349, 450);*/

      GUI.draw.beginFill(PixelTanks.userData.color);
      GUI.draw.alpha = .7;
      GUI.draw.drawRect(500, 900+(Math.min((Date.now()-this.timers.items[0].date.getTime())/this.timers.items[0].cooldown, 1))*100, 100, 100);
      GUI.draw.drawRect(666, 900+(Math.min((Date.now()-this.timers.items[1].date.getTime())/this.timers.items[1].cooldown, 1))*100, 100, 100);
      GUI.draw.drawRect(832, 900+(Math.min((Date.now()-this.timers.items[2].date.getTime()) / this.timers.items[2].cooldown, 1))*100, 100, 100);
      GUI.draw.drawRect(998, 900+(Math.min((Date.now()-this.timers.items[3].date.getTime()) / this.timers.items[3].cooldown, 1))*100, 100, 100);
      GUI.draw.drawRect(358, 900+(Math.min((Date.now()-this.timers.class.date.getTime())/this.timers.class.cooldown, 1))*100, 100, 100);
      GUI.draw.drawRect(1142, 900+(Math.min((Date.now()-this.timers.powermissle.getTime())/10000, 1))*100, 100, 100);
      GUI.draw.endFill();
      GUI.draw.alpha = 1;
      /*
      GUI.draw.globalAlpha = 1;
      GUI.draw.fillStyle = '#ffffff';
      GUI.draw.fillText(PixelTanks.userData.boosts, 136, 470);
      GUI.draw.fillText(PixelTanks.userData.toolkits, 183 + 36, 470);
      GUI.draw.fillText(PixelTanks.userData.blocks, 266 + 35, 470);
      GUI.draw.fillText(PixelTanks.userData.flashbangs, 349 + 35, 470);

      GUI.draw.translate(-PixelTanks.offset, 0);

      GUI.draw.textAlign = 'left';
      GUI.draw.font = '10px Neuropol';
      GUI.draw.fillText('Kills Streak: ' + this.kills, 10, 15);
      GUI.draw.fillText('Crates: ' + this.crates, 10, 35);
      GUI.draw.fillText('Experience: ' + this.xp, 10, 55);
      GUI.draw.textAlign = 'center';
      GUI.draw.fillText(this.stats, 250, 15);
      GUI.draw.textAlign = 'left';
*/
      var l = 0, len;
      if (this.showChat || this.hostupdate.logs.length < 3) {
        len = this.hostupdate.logs.length;
      } else {
        len = 3;
      }

      while (l<len) {
        GUI.drawText(this.hostupdate.logs[l].m, GUI.root.pivot.x, GUI.root.pivot.y+800-l*24, 24, this.hostupdate.logs[l].c, 0);
        l++;
      }
      if (this.showChat) GUI.drawText(this.msg, GUI.root.pivot.x, GUI.root.pivot.y+824, 24, '#ffffff', 0);
    }

    chat(e) {
      if (e.key.length === 1) {
        this.msg += e.key;
      } else if (e.keyCode === 8) {
        this.msg = this.msg.slice(0, -1);
      } else if (e.keyCode === 13) {
        if (this.msg !== '') {
          if (this.msg.split('')[0] === '/') {
            var command = this.msg.split(' ')[0]
            if (command === '/emote') {
              if (this.msg.split(' ')[1] === 'set') {
                var data = this.msg.split(' ')[2];
                data = data.split('~');
                if ((new Number(data[1]) >= 5 && new Number(data[1]) <= 9) || data[1] == 0) {
                  if (data[1] == 0) {
                    data[1] = 10;
                  }
                  PixelTanks.userData['emote' + (data[1] - 4)] = data[0];
                } else {
                  alert('invalid everything. bad idot.')
                }
              } else {
                this.emote(this.msg.split(' ')[1])
              }
            } else if (['/createteam', '/join', '/leave', '/accept', '/ban', '/unban', '/kick', '/kill', '/target'].includes(command)) {
              this.socket.send({
                type: 'command',
                data: this.msg.split(' '),
              });
            } else {
              alert('Command nonexistant. Use /emote emotename');
            }
          } else {
            this.socket.send({
              type: 'chat',
              msg: this.msg,
            });
          }
          this.msg = '';
        }
        this.showChat = false;
      }
    }

    keydown(e) {
      e = e || event;
      e.preventDefault();
      if (this.helper[e.keyCode] !== 'pressed') {
        if (this.showChat) {
          this.chat(e);
          return;
        }
        var cooldown = 15;
        if (this.tank.in) {
          cooldown = 12;
        }
        this.keyStart(e);
        if (this.paused) return;
        this.keyLoop(e);
        this.intervals2[e.keyCode] = setInterval(function() {
          this.tank.ba = !this.tank.ba;
          var left = this.left;
          var up = this.up;
          if (left === null) {
            if (up === null) {} else if (up) {
              this.tank.br = 180;
            } else if (!up) {
              this.tank.br = 0;
            }
          } else if (left) {
            if (up === null) {
              this.tank.br = 90;
            } else if (up) {
              this.tank.br = 135;
            } else if (!up) {
              this.tank.br = 45;
            }
          } else if (!left) {
            if (up === null) {
              this.tank.br = 270;
            } else if (up) {
              this.tank.br = 225;
            } else if (!up) {
              this.tank.br = 315;
            }
          }
        }.bind(this), 100);
        this.intervals[e.keyCode] = setInterval(this.keyLoop.bind(this), cooldown, e);
      }
      this.helper[e.keyCode] = 'pressed';
    }

    keyup(e) {
      if (this.paused) return;
      e.preventDefault();
      clearInterval(this.intervals[e.keyCode]);
      clearInterval(this.intervals2[e.keyCode]);
      if (e.keyCode == 65 || e.keyCode == 68) {
        this.left = null;
      }
      if (e.keyCode == 87 || e.keyCode == 83) {
        this.up = null;
      }
      this.helper[e.keyCode] = false;
    }

    mousemove(e) {
      var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      var targetX = x - window.innerWidth/2, targetY = y - window.innerHeight/2;
      var rotation = this.toAngle(targetX, targetY);
      this.tank.r = Math.round(rotation);
      this.mouse = {
        x: targetX,
        y: targetY,
      }
      if (this.SETTINGS.fps) {
        this.send();
      }
    }

    toAngle(x, y) {
      var angle = Math.atan2(x, y) * 180 / Math.PI
      angle = -angle //-90;
      if (angle < 0) {
        angle += 360;
      }
      if (angle >= 360) {
        angle -= 360;
      }
      return angle;
    }

    toPoint(angle) {
      var theta = (-angle) * Math.PI / 180;
      var y = Math.cos(theta);
      var x = Math.sin(theta);
      if (x < 0) {
        if (y < 0) {
          return {
            x: -1,
            y: Math.round(-Math.abs(y / x) * 1000) / 1000,
          };
        } else {
          return {
            x: -1,
            y: Math.round(Math.abs(y / x) * 1000) / 1000,
          };
        }
      } else {
        if (y < 0) {
          return {
            x: 1,
            y: Math.round(-Math.abs(y / x) * 1000) / 1000,
          };
        } else {
          return {
            x: 1,
            y: Math.round(Math.abs(y / x) * 1000) / 1000,
          };
        }
      }
    }

    mousedown(e) {
      var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - ((window.innerWidth-window.innerHeight*1.6)/2)*PixelTanks.resizer;
      var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      x /= PixelTanks.resizer;
      y /= PixelTanks.resizer;

      var cooldown = 0;
      if (this.fireType == 1) {
        cooldown = 200;
      }
      if (this.fireType == 2) {
        cooldown = 600;
      }
      if (this.fireType == 3) {
        cooldown = 400;
      } 
      if (this.fireType == 4) {
        cooldown = 66;
      }
      if (this.fireType == 5) {
        this.fire(e.button);
        return;
      }

      if (this.canFire) {
        this.fire(e.button);
        this.canFire = false;
        setTimeout(function() {
          this.canFire = true;
        }.bind(this), cooldown);
      }

      clearInterval(this.fireInterval);
      this.fireInterval = setInterval(this.fire.bind(this), cooldown, e.button);
    }

    mouseup(e) {
      this.tank.fi = [];
      clearInterval(this.fireInterval);
    }

    fire(type) {
      var fireType = this.fireType;
      if (type === 'grapple' || type === 'megamissle') {
        fireType = 1;
      } else if (type === 'dynamite') {
        fireType = 1;
      } else if (type == 2) {
        if (this.canPowermissle) {
          type = 'powermissle';
          fireType = 1;
          this.canPowermissle = false;
          this.timers.powermissle = new Date();
          var cooldown = 10000;
          if (PixelTanks.userData.kit === 'cooldown') {
            cooldown *= .9;
          }
          setTimeout(function() {
            this.canPowermissle = true;
          }.bind(this), cooldown);
        } else {
          if (this.fireType == 1) {
            type = 'bullet';
          } else if (this.fireType == 2) {
            type = 'shotgun';
          }
        }
      } else {
        if (this.fireType == 1) {
          type = 'bullet';
        } else if (this.fireType == 2) {
          type = 'shotgun';
        }
      }

      if (PixelTanks.userData.class === 'medic' && type === 'powermissle') type = 'healmissle';

      if (fireType == 1) {
        var bullet = this.toPoint(this.tank.r);
        bullet.t = type;
        bullet.r = this.tank.r;
        this.tank.fi.push(bullet);
      } else if (fireType == 2) { // REVISE with while loop
        var l = -10;
        while (l < 15) {
          var bullet = this.toPoint(this.tank.r + l);
          bullet.t = type;
          bullet.r = this.tank.r + l;
          this.tank.fi.push(bullet);
          l += 5;
        }
      }
    
      if (this.SETTINGS.fps) {
        this.send();
      }
    }

    collision(x, y) {

      var l = 0, team;
      while (l < this.hostupdate.tanks.length) {
        if (this.hostupdate.tanks[l].u === PixelTanks.user.username) {
          team = this.hostupdate.tanks[l].t.split(':')[1].replace('@leader', '').replace('@requestor#', '');
          if (this.hostupdate.tanks[l].ded) return true;
        }
        l++;
      }

      if (x < 0 || y < 0 || x + 40 > 1500 || y + 40 > 1500) {
        return false;
      }

      if (this.tank.in && this.tank.i) return true;
      var l = 0,
        blocks = this.hostupdate.blocks,
        len = blocks.length;
      while (l < len) {
        if ((x > blocks[l].x || x + 40 > blocks[l].x) && (x < blocks[l].x + 50 || x + 40 < blocks[l].x + 50) && (y > blocks[l].y || y + 40 > blocks[l].y) && (y < blocks[l].y + 50 || y + 50 < blocks[l].y + 50)) {
          if ((blocks[l].t === 'fortress' && blocks[l].o.split(':')[1] === team) || blocks[l].t === 'heal') {} else if (blocks[l].c) {
            return false;
          }
        }
        l++;
      }
      return true;
    }

    playAnimation(id) {
      this.tank.a = {
        i: id,
        f: 0,
      };
      clearInterval(this.animationInterval);
      this.animationInterval = setInterval(function() {
        if (this.tank.a.f === PixelTanks.images.animations[id].frames) {
          clearInterval(this.animationInterval);
          setTimeout(function() {
            this.tank.a = false;
          }.bind(this), PixelTanks.images.animations[id].speed);
        } else {
          this.tank.a.f++;
        }
      }.bind(this), PixelTanks.images.animations[id].speed);
    }

    item(id, slot) {
      /*
        case 32:
          if (this.canBlock) {
            if (PixelTanks.userData.blocks > 0) {
              this.canBlock = false;
              this.timers.block = new Date();
              PixelTanks.userData.blocks -= 1;
              this.tank.pl = true; // place scaffolding
              if (PixelTanks.userData.class == 'builder') {
                this.tank.st = 'gold';
              } else {
                this.tank.st = 'weak';
              }
              var cooldown = 5000;
              if (PixelTanks.userData.kit == 'cooldown') {
                cooldown *= .9;
              }
              setTimeout(function() {
                this.canBlock = true;
              }.bind(this), cooldown);
            }
          }
          break;
        case 69:
          if (PixelTanks.userData.flashbangs > 0) {
            if (this.canFlashbang) {
              PixelTanks.userData.flashbangs -= 1;
              this.tank.fl = true;
              this.canFlashbang = false;
              this.timers.flashbang = new Date();
              var cooldown = 40000;
              if (PixelTanks.userData.kit == 'cooldown') {
                cooldown *= .9;
              }
              setTimeout(function() {
                this.canFlashbang = true;
              }.bind(this), cooldown);
            }
          }
      */
      var key = {
        duck_tape: [function() {
          this.tank.ta = true;
          this.playAnimation('tape');
        }, 30000, false],
        super_glu: [function() {
          this.tank.gl = true;
        }, 40000, false],
        shield: [function() {
          this.tank.sh = true;
        }, 30000, false],
        weak: [function() {
          if (PixelTanks.userData.blocks > 0) {
            PixelTanks.userData.blocks--;
            this.tank.pl = true; // place scaffolding
            this.tank.st = PixelTanks.userData.class === 'builder' ? 'gold' : 'weak';
          }
        }, 3000, false],
        strong: [function() {
          if (PixelTanks.userData.blocks > 0) {
            PixelTanks.userData.blocks--;
            this.tank.pl = true;
            this.tank.st = PixelTanks.userData.class === 'builder' ? 'gold' : 'strong';
          }
        }, 7000, false],
        spike: [function() {
          if (PixelTanks.userData.blocks > 0) {
            PixelTanks.userData.blocks--;
            this.tank.pl = true;
            this.tank.st = 'spike';
          }
        }, 10000, false],
        flashbang: [function() {
          if (PixelTanks.userData.flashbangs > 0) {
            PixelTanks.userData.flashbangs--;
            this.tank.fl = true;
          }
        }, 20000, false],
        bomb: [function() {
          if (true) {
            this.tank.bo = true;
          }
        }, 10000, false],
        power_bomb: [function() {
          if (true) {
            this.tank.po = true;
          }
        }, 10000, false],
        mine: [function() {
          if (true) {
            this.tank.pl = true;
            this.tank.st = 'mine';
          }
        }, 5000, false],
        dynamite: [function() {
          if (!this['canItem'+slot]) {
            this.tank.dy = true;
          } else {
            this.fire('dynamite');
            this['canItem'+slot] = false;
            this.timers.items[slot].cooldown = 25000;
            this.timers.items[slot].date = new Date();
            setTimeout(function() {
              this['canItem'+slot] = true;
            }.bind(this), 25000);
          }
        }, 25000, true],
        airstrike: [function() {
          if (true) {
            this.tank.as = {
              x: this.mouse.x+this.tank.x-50,
              y: this.mouse.y+this.tank.y-50,
            }; // airstrike
          }
        }, 40000, false],
        fortress: [function() {
          if (true) {
            this.tank.pl = true;
            this.tank.st = 'fortress';
          }
        }, 30000, false],
      }
      this.useItem(key[id][0], key[id][1], slot, key[id][2]);
    }

    useItem(enable, cooldown, slot, c) {
      if (c) {
        enable = enable.bind(this);
        enable();
        return;
      }
      if (this['canItem'+slot]) {
        enable = enable.bind(this);
        enable();
        this.timers.items[slot].cooldown = cooldown;
        this.timers.items[slot].date = new Date();
        this['canItem'+slot] = false;
        setTimeout(function() {
          this['canItem'+slot] = true;
        }.bind(this), cooldown);
      }
    }

    keyStart(e) {
      if (this.paused && e.keyCode !== 22) return;
      switch (e.keyCode) {
        case PixelTanks.userData.settings.item1:
          this.item(PixelTanks.userData.item1, 0);
          break;
        case PixelTanks.userData.settings.item2:
          this.item(PixelTanks.userData.item2, 1);
          break;
        case PixelTanks.userData.settings.item3:
          this.item(PixelTanks.userData.item3, 2);
          break;
        case PixelTanks.userData.settings.item4:
          this.item(PixelTanks.userData.item4, 3);
          break;
        case 53:
          this.emote(PixelTanks.userData.emote1);
          break;
        case 54:
          this.emote(PixelTanks.userData.emote2);
          break;
        case 55:
          this.emote(PixelTanks.userData.emote3);
          break;
        case 56:
          this.emote(PixelTanks.userData.emote4);
          break;
        case 57:
          this.emote(PixelTanks.userData.emote5)
          break;
        case 48:
          this.emote(PixelTanks.userData.emote6)
          break;
        case 13:
          this.showChat = true;
          break;
        case PixelTanks.userData.settings.fire1:
          this.fireType = 1;
          clearInterval(this.fireInterval);
          break;
        case PixelTanks.userData.settings.fire2:
          this.fireType = 2;
          clearInterval(this.fireInterval);
          break;
        case 9:
          if (this.fireType === 2) {
            this.fireType = 1;
          } else {
            this.fireType++;
          }
          clearInterval(this.fireInterval);
          break;
        case 82:
          if (this.grapples > 0) {
            this.fire('grapple');
            this.grapples--;
            this.canGrapple = false;
            setTimeout(function() {
              this.canGrapple = true;
            }.bind(this), 200)
            if (this.grapples === 0) {
              setTimeout(function() {
                this.grapples = 1;
              }.bind(this), 5000);
            }
          }
          break;
        case 81:
          if (this.halfSpeed) {
            this.tank.to = true;
            this.halfSpeed = false;
          } else {
            if (this.canToolkit && PixelTanks.userData.toolkits > 0) {
              PixelTanks.userData.toolkits -= 1;
              if (PixelTanks.userData.class !== 'medic') {
                this.halfSpeed = true;
                setTimeout(function() {
                  this.halfSpeed = false;
                }.bind(this), 7500);
              } 
              this.tank.to = true;
              this.canToolkit = false;
              this.timers.toolkit = new Date();
              setTimeout(function() {
                this.canToolkit = true;
              }.bind(this), 30000);
              this.playAnimation('toolkit');
            }
          }
          break;
        case 70:
          if (PixelTanks.userData.class === 'stealth') {
            if (this.canInvis && !this.tank.in) {
              this.tank.in = true;
              this.canInvis = false;
              this.timers.class.date = new Date();
              this.timers.class.cooldown = 20000;
              clearTimeout(this.invis);
              this.invis = setTimeout(function() {
                this.tank.in = false;
                this.invis = setTimeout(function() {
                  this.canInvis = true;
                }.bind(this), 20000);
              }.bind(this), 20000);
            } else if (this.tank.in) {
              this.tank.in = false;
              this.canInvis = true;
            }
          } else if (PixelTanks.userData.class === 'normal') {
            // add sheidls ehre for idots
          } else if (PixelTanks.userData.class == 'tactical') {
            if (this.canMegamissle) {
              this.fire('megamissle');
              this.canMegamissle = false;
              this.timers.class.date = new Date();
              this.timers.class.cooldown = 20000
              setTimeout(function() {
                this.canMegamissle = true;
              }.bind(this), 20000);
            }
          } else if (PixelTanks.userData.class == 'builder') {
            if (this.canTurret) {
              this.canTurret = false;
              this.tank.tu = true;
              this.timers.class.date = new Date();
              this.timers.class.cooldown = 40000;
              setTimeout(function() {
                this.canTurret = true;
              }.bind(this), 40000);
            }
          } else if (PixelTanks.userData.class === 'warrior') {
            if (this.canBuff) {
              this.canBuff = false;
              this.tank.bu = true;
              this.tank.to = false;
              this.timers.class.date = new Date();
              this.timers.class.cooldown = 30000;
              setTimeout(function() {
                this.canBuff = true;
              }.bind(this), 30000);
            }
          } else if (PixelTanks.userData.class === 'medic') {
            if (this.canHeal) {
              this.canHeal = false;
              this.tank.pl = true;
              this.tank.st = 'heal';
              this.timers.class.date = new Date();
              this.timers.class.cooldown = 30000;
              setTimeout(function() {
                this.canHeal = true;
              }.bind(this), 30000);
            }
          }
          break;
        case 27:
          this.paused = !this.paused;
          if (this.paused) {
            GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(0, 0, 1600, 1000);
          } else {
            Menus.removeListeners();
          }
          break;
        case 18:
          document.write(JSON.stringify(this.hostupdate));
          break;
      }
    }

    keyLoop(e) {
      switch (e.keyCode) {
        case 68:
          if (this.collision(this.tank.x + this.speed, this.tank.y) && !this.tank.to) {
            this.tank.x += (this.halfSpeed) ? this.speed / 2 : this.speed;
            this.left = false;
          } else {
            this.left = null;
          }
          break;
        case 87:
          if (this.collision(this.tank.x, this.tank.y - this.speed) && !this.tank.to) {
            this.tank.y -= (this.halfSpeed) ? this.speed / 2 : this.speed;
            this.up = true;
          } else {
            this.up = null;
          }
          break;
        case 65:
          if (this.collision(this.tank.x - this.speed, this.tank.y) && !this.tank.to) {
            this.tank.x -= (this.halfSpeed) ? this.speed / 2 : this.speed;
            this.left = true;
          } else {
            this.left = null;
          }
          break;
        case 83:
          if (this.collision(this.tank.x, this.tank.y + this.speed) && !this.tank.to) {
            this.tank.y += (this.halfSpeed) ? this.speed / 2 : this.speed;
            this.up = false;
          } else {
            this.up = null;
          }
          break;
        case 16:
          if (this.canBoost) {
            this.speed = 8;
            this.canBoost = false;
            this.tank.i = true;
            setTimeout(function() {
              this.speed = 2;
              this.tank.i = false;
            }.bind(this), 500);
            var cooldown = 5000;
            if (PixelTanks.userData.kit == 'cooldown') {
              cooldown *= .9;
            }
            setTimeout(function() {
              this.canBoost = true;
            }.bind(this), cooldown);
          }
          break;
      }
      if (this.SETTINGS.fps) {
        this.send();
      }
    }

    emote(id) {
      clearInterval(this.emoteAnimation);
      clearTimeout(this.emoteTimeout);
      if (PixelTanks.images.emotes[id].type === 0) { // loop emote
        this.tank.e = {
          a: id,
          f: 0,
        };
        this.emoteAnimation = setInterval(function() {
          if (this.tank.e.f != PixelTanks.images.emotes[id].frames) {
            this.tank.e.f++;
          } else {
            this.tank.e.f = 0;
          }
        }.bind(this), 50);
        this.emoteTimeout = setTimeout(function() {
          clearInterval(this.emoteAnimation);
          this.tank.e = null;
        }.bind(this), 5000);
      } else if (PixelTanks.images.emotes[id].type === 1) { // single run emote
        this.tank.e = {
          a: id,
          f: 0,
        };
        this.emoteAnimation = setInterval(function() {
          if (this.tank.e.f != PixelTanks.images.emotes[id].frames) {
            this.tank.e.f++;
          } else {
            clearInterval(this.emoteAnimation);
            setTimeout(function() {
              this.tank.e = null;
            }.bind(this), 1500);
          }
        }.bind(this), 50);
      } else {
        this.tank.e = {
          a: id,
          f: 0,
        }
        this.emoteTimeout = setTimeout(function() {
          this.tank.e = null;
        }.bind(this), 5000);
      }
    }

    send() {
      this.ops++;
      this.socket.send({
        username: sessionStorage.username,
        type: 'update',
        data: {
          x: this.tank.x, // x [U, J]
          y: this.tank.y, // y [U, J]
          e: this.tank.e, // [U]
          br: this.tank.br, // leftright [U, J]
          r: this.tank.r, // rotation [U, J]
          to: this.tank.to, // toolkit [U]
          ta: this.tank.ta, // duck tape [U]
          gl: this.tank.gl, // glu [U]
          pl: this.tank.pl, // place scaffolding [U]
          st: this.tank.st, // scaffolding type
          ba: this.tank.ba, // base image stage [U, J]
          fl: this.tank.fl, // flashbang fired [U]
          bo: this.tank.bo, // bumb fired [U]
          po: this.tank.po, // power-bumb fired [U]
          i: this.tank.i, // immune [U]
          in: this.tank.in, // invis  [U]
          fi: this.tank.fi, // fire or not
          tu: this.tank.tu, // summoner turret
          bui: this.tank.bui, // builder block
          a: this.tank.a, // animation to play on tank :D
          bu: this.tank.bu, // warrior buff
          mi: this.tank.mi, // mines while boost TEMP DISABLED
          dy: this.tank.dy, // dynamite
          sh: this.tank.sh, // shields :D
          as: this.tank.as, // airstrike
        },
      });
      this.tank.st = null;
      this.tank.blockShield = false;
      this.tank.fi = [];
      this.tank.mi = false;
      this.tank.to = false;
      this.tank.ta = false;
      this.tank.pl = false;
      this.tank.tu = false;
      this.tank.fl = false;
      this.tank.bo = false;
      this.tank.po = false;
      this.tank.bu = false;
      this.tank.bui = false;
      this.tank.dy = false;
      this.tank.gl = false;
      this.tank.sh = false;
      this.tank.as = false;
    }

  }

  class SinglePlayerTank {
    constructor() {

      this.m = PixelTanks.userData.material;
      this.world = PixelTanks.user.world;

      this.b = true;
      this.p = 0;
      this.h = (this.m * 50 + 300);
      this.r = 0;
      this.x = 0;
      this.y = 0;
      this.u = true;
      this.l = true;
      this.i = false;
      this.in = false;
      this.class = PixelTanks.userData.class;
      this.s = 0;

      this.canFlashbang = true;
      this.canBlock = true;
      this.canBoost = true;
      this.canToolkit = true;
      this.canInvis = true;
      this.canFire = true;
      this.canChangeInvisStatus = true;
      this.canPowermissle = true;
      this.canMegamissle = true;
      this.canGrapple = true;

      this.speed = 2;
      this.fireType = 1;
      this.team = 'Player';

      this.intervals = [];
      this.intervals2 = [];
      this.helper = [];

      document.addEventListener('keydown', this.keydown.bind(this)); // revise | add a addListeners() function ?
      document.addEventListener('keyup', this.keyup.bind(this));
      document.addEventListener('mousemove', this.mousemove.bind(this));
      document.addEventListener('mousedown', this.mousedown.bind(this));
      document.addEventListener('mouseup', this.mouseup.bind(this));
    }

    damage(s) {
      if (this.s > 0) {
        this.s -= 1;
        return;
      }

      if (this.i) {
        return;
      }

      this.health -= s.damage;

      if (this.health <= 0) {
        setTimeout(defeat, 20);
        PixelTanks.user.world.endGame();
      }
    }

    draw() {

      if (this.in) {
        GUI.draw.globalAlpha = .5;
      }

      GUI.draw.translate(this.x + 20, this.y + 20)
      GUI.draw.rotate(this.br * Math.PI / 180);

      var key = {
        0: 'red',
        1: 'iron',
        2: 'diamond',
        3: 'dark',
        4: 'light',
      };

      GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[this.m]].bottom[this.b ? 0 : 1], -20, -20);

      GUI.draw.rotate(-this.br * Math.PI / 180);
      GUI.draw.translate((-this.x - 20), (-this.y - 20));

      GUI.draw.translate(this.x + 20, this.y + 20);
      GUI.draw.rotate(this.r * Math.PI / 180);

      GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[this.m]].top, -20, -20);

      if (PixelTanks.userData.cosmetic) {
        var l = 0;
        while (l < PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
          if (PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].name == PixelTanks.userData.cosmetic) {
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].image, -20, -20 + this.p);
          }
          l++;
        }
      }

      GUI.draw.rotate(-(this.r * Math.PI / 180));
      GUI.draw.translate(-this.x - 20, -this.y - 20);

      // Health Bar!
      GUI.draw.fillStyle = '#000000';
      GUI.draw.fillRect(this.x, this.y + 50, 40, 5);
      GUI.draw.fillStyle = '#90EE90';
      GUI.draw.fillRect(this.x + 2, this.y + 51, 36 * this.h / ((this.m * 50 + 300) * (1 + .02 * PixelTanks.userData.rank)), 3);

      if (this.s > 0) {
        GUI.draw.strokeStyle = '#7DF9FF';
        GUI.draw.globalAlpha = .2;
        GUI.draw.lineWidth = 5;
        GUI.draw.beginPath();
        GUI.draw.arc(this.x + 20, this.y + 20, 33, 0, Math.PI * 2);
        GUI.draw.fill();
        GUI.draw.globalAlpha = 1;
      }

      if (this.in) {
        GUI.draw.globalAlpha = 1;
      }
    }

    keydown(e) {
      e = e || event;
      e.preventDefault();
      if (this.helper[e.keyCode] !== 'pressed') {
        var cooldown = 15;
        if (this.in) {
          cooldown = 12;
        }
        this.keyHandler(e);
        this.intervals2[e.keyCode] = setInterval(function() {
          this.ba = !this.ba;
          var left = this.left;
          var up = this.up;
          if (left === null) {
            if (up === null) {
              // no angle
            } else if (up) {
              this.br = 180;
            } else if (!up) {
              this.br = 0;
            }
          } else if (left) {
            if (up === null) {
              this.br = 90;
            } else if (up) {
              this.br = 135;
            } else if (!up) {
              this.br = 45;
            }
          } else if (!left) {
            if (up === null) {
              this.br = 270;
            } else if (up) {
              this.br = 225;
            } else if (!up) {
              this.br = 315;
            }
          }
        }.bind(this), 20);
        this.intervals[e.keyCode] = setInterval(this.keyHandler.bind(this), cooldown, e);
      }
      this.helper[e.keyCode] = 'pressed';
    }

    keyup(e) {
      e.preventDefault();
      clearInterval(this.intervals[e.keyCode]);
      clearInterval(this.intervals2[e.keyCode]);
      if (e.keyCode == 65 || e.keyCode == 68) {
        this.left = null;
      }
      if (e.keyCode == 87 || e.keyCode == 83) {
        this.up = null;
      }
      this.helper[e.keyCode] = false;
    }

    mousemove(e) {
      var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - ((window.innerWidth-window.innerHeight*1.6)/2)*PixelTanks.resizer;
      var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      x /= PixelTanks.resizer;
      y /= PixelTanks.resizer;
      var targetX = x - (250 + PixelTanks.offset),
        targetY = y - 250;
      var rotation = this.toAngle(targetX, targetY);
      this.r = Math.round(rotation);
    }

    toAngle(x, y) {
      var angle = Math.atan2(x, y) * 180 / Math.PI
      angle = -angle //-90;
      if (angle < 0) {
        angle += 360;
      }
      if (angle >= 360) {
        angle -= 360;
      }
      return angle;
    }

    toPoint(angle) {
      var theta = (-angle) * Math.PI / 180;
      var y = Math.cos(theta);
      var x = Math.sin(theta);
      if (x < 0) {
        if (y < 0) {
          return {
            x: -1,
            y: Math.round(-Math.abs(y / x) * 1000) / 1000,
          };
        } else {
          return {
            x: -1,
            y: Math.round(Math.abs(y / x) * 1000) / 1000,
          };
        }
      } else {
        if (y < 0) {
          return {
            x: 1,
            y: Math.round(-Math.abs(y / x) * 1000) / 1000,
          };
        } else {
          return {
            x: 1,
            y: Math.round(Math.abs(y / x) * 1000) / 1000,
          };
        }
      }
    } // REVISE maybe make a Math class for this (not named Math tho)

    mousedown(e) {
      var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - ((window.innerWidth-window.innerHeight*1.6)/2)*PixelTanks.resizer;
      var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      x /= PixelTanks.resizer;
      y /= PixelTanks.resizer;

      var cooldown = 0;
      if (this.fireType == 1) {
        cooldown = 200;
      }
      if (this.fireType == 2) {
        cooldown = 600;
      }
      if (this.fireType == 3) {
        cooldown = 600;
      }
      if (this.fireType == 4) {
        cooldown = 50;
      }
      if (this.fireType == 5) {
        this.fire(e.button);
        return;
      }

      if (this.canFire) {
        this.fire(e.button);
        this.canFire = false;
        setTimeout(function() {
          this.canFire = true;
        }.bind(this), cooldown);
      }

      clearInterval(this.fireInterval);
      this.fireInterval = setInterval(this.fire.bind(this), cooldown, e.button);
    }

    mouseup(e) {
      clearInterval(this.fireInterval);
    }

    collision(x, y) {
      if (x < 0 || y < 0 || x + 40 > 1500 || y + 40 > 1500) {
        return false;
      }
      if (this.in && this.i) return true;
      var l = 0;
      while (l<this.world.b.length) { // REVISE add the world object to be a part of the objects properties (this.world) 
        
        l++;
      }
      var l = 0, b = this.world.b;
      while (l<b.length) {
        if ((x > b[l].x || x + 40 > b[l].x) && (x < b[l].x + 50 || x + 40 < b[l].x + 50) && (y > b[l].y || y + 40 > b[l].y) && (y < b[l].y + 50 || y + 50 < b[l].y + 50)) {
          if (b[l].c) {
            return false;
          }
        }
        l++;
      }
      return true;
    }

    keyHandler(e) {
      e = e || event;
      switch (e.keyCode) {
        case 68:
          if (this.collision(this.x + this.speed, this.y)) {
            this.x += this.speed;
            this.left = false;
          } else {
            this.left = null;
          }
          break;
        case 87:
          if (this.collision(this.x, this.y - this.speed)) {
            this.y -= this.speed;
            this.up = true;
          } else {
            this.up = null;
          }
          break;
        case 65:
          if (this.collision(this.x - this.speed, this.y)) {
            this.x -= this.speed;
            this.left = true;
          } else {
            this.left = null;
          }
          break;
        case 83:
          if (this.collision(this.x, this.y + this.speed)) {
            this.y += this.speed;
            this.up = false;
          } else {
            this.up = null;
          }
          break;
        case 16:
          break;
        case 81:
          break;
        case 32:
          break;
        case 69:
          break;
        case 70:
          break;
        case 76:
          break;
        case 9:
          break;
        case 49:
          this.fireType = 1;
          clearInterval(tankSupport);
          break;
        case 50:
          //if (PixelTanks.userData.fire2) {
          this.fireType = 2;
          clearInterval(tankSupport);
          //}
          break;
        case 51:
          //if (PixelTanks.userData.fire3) {
          this.fireType = 3;
          clearInterval(tankSupport);
          //}
          break;
        case 17:
          break;
      }
    }

    fire(type) {
      var fireType = this.fireType;

      if (type === 'megamissle') {
        fireType = 1;
      } else if (type == 2) {
        if (this.canPowermissle) {
          type = 'powermissle';
          fireType = 1;
          this.canPowermissle = false;
          var cooldown = 10000;
          if (PixelTanks.userData.kit === 'cooldown') {
            cooldown *= .9;
          }
          setTimeout(function() {
            this.canPowermissle = true;
          }.bind(this), cooldown);
        } else {
          if (this.fireType == 1) {
            type = 'bullet';
          } else if (this.fireType == 2) {
            type = 'shotgun';
          }
        }
      } else if (type === 'dynamite') {
        type = 'dynamite';
        fireType = 1;
      } else {
        if (this.fireType == 1) {
          type = 'bullet';
        } else if (this.fireType == 2) {
          type = 'shotgun';
        }
      }

      var xd = [],
        yd = [],
        ro = [];

      if (fireType == 1) {
        var data = this.toPoint(this.r);
        xd.push(data.x);
        yd.push(data.y);
        ro.push(this.r);
      } else if (fireType == 2) {
        var offset = -10;
        while (offset !== 10) {
          var data = this.toPoint(this.r + offset);
          xd.push(data.x);
          yd.push(data.y);
          ro.push(this.r);
          offset += 5;
        }
      } else if (fireType == 3) {
        var data = this.toPoint(this.tank.r);
        var l = 0;
        while (l < 5) {
          xd.push(data.x);
          yd.push(data.y);
          ro.push(this.r);
          l++;
        }
      } else if (fireType == 4) {
        var data = this.toPoint(this.tank.r);
        var l = 0;
        while (l < 5) {
          xd.push(data.x);
          yd.push(data.y);
          ro.push(this.r);
          l++;
        }
      }
      var l = 0;
      while (l < xd.length) {
        PixelTanks.user.world.s.push(new Shot(this.x+20, this.y+20, xd[l], yd[l], type, ro[l], PixelTanks.user.username, PixelTanks.user.world));
        l++;
      }
    }
  }

  class World {
    constructor() {
      this.LEVELS = {
        1: ['                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '         111111111            ', '         122222221            ', '         12#####21            ', '         12#===#21            ', '         12#=@=#21            ', '         12#= =#21            ', '         12## ##21            ', '         1222 2221            ', '         1111 1111            ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              '],

        2: [],
        3: [],
        4: [],
        5: [],
      };
      this.xp = 0;
      this.coins = 0;
      this.foes = 0;
      this.i = [];
      this.t = [];
      this.s = [];
      this.b = [];
      this.ai = [];
    }

    pauseGame() {

    }

    endGame() {
      window.removeEventListener('blur', PixelTanks.user.world.leaveWindow);
      document.removeEventListener('visibilitychange', PixelTanks.user.world.tabHandler);
      document.removeEventListener('click', pauseOnClick);
      document.removeEventListener('keydown', PixelTanks.user.world.pauseHandler);
      this.removeListeners();
      var l = 0;
      while (l < PixelTanks.user.tank.intervals.length) {
        if (PixelTanks.user.tank.intervals[l]) {
          clearInterval(PixelTanks.user.tank.intervals[l]);
        }
        l++;
      }
      gameHelper();
    }

    tick() {
      var l = 0;
      while (l < this.s.length) {
        this.s[l].update();
        l++;
      }

      if (PixelTanks.user.tank.p != 0) {
        PixelTanks.user.tank.p += 1;
      }
      // ai here
    }

    init() {
      this.intervals();
      PixelTanks.user.tank = new SinglePlayerTank();
      this.levelReader(this.LEVELS[this.level]);
    }

    levelReader(array) {
      this.map = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
      this.borders = [0, 1500, 0, 1500];
      for (var l = 0; l < array.length; l++) {
        for (var q = 0; q < array[l].split('').length; q++) {
          var p = array[l].split(''); // Block key: # = invincible, 1 = weak, 2 = strong, @ = player, A = Area, C = Condensed, W = Weak Tanks, I = normal T = turret, B = Invis Area, D = Invis Condensed, X = Invis Weak, L = loot create, U = PowerBullet Turret, V = Mega Bullet Turret
          if (p[q] == '#') {
            this.map[l] += '1';
            this.b.push(new Block(q * 50, l * 50, Infinity, 'barrier', this));
          } else if (p[q] == '1') {
            this.map[l] += '1';
            this.b.push(new Block(q * 50, l * 50, 120, 'weak', this))
          } else if (p[q] == 'G') {
            this.map[l] += '1';
            this.b.push(new Block(q*50, l**50, 360, 'gold', this));
          } else if (p[q] == '2') {
            this.map[l] += '1';
            this.b.push(new Block(q * 50, l * 50, 240, 'strong', this));
          } else if (p[q] == '=') {
            this.map[l] += '1';
            this.b.push(new Block(q * 50, l * 50, Infinity, 'void', this));
          } else if (p[q] == 'I') {
            this.map[l] += '0';
            createAi(q * 50, l * 50, false, 1, false, null, 'Eval');
          } else if (p[q] == 'W') {
            PixelTanks.user.world.map[l] += '0';
            createAi(q * 50, l * 50, false, 0, false, null, 'Eval');
          } else if (p[q] == 'A') {
            PixelTanks.user.world.map[l] += '0';
            createAi(q * 50, l * 50, false, 2, false, null, 'Eval');
          } else if (p[q] == 'C') {
            PixelTanks.user.world.map[l] += '0';
            createAi(q * 50, l * 50, false, 3, false, null, 'Eval');
          } else if (p[q] == 'T') {
            createAi(q * 50, l * 50, 'turret', 1, false, null, 'Eval');
          } else if (p[q] == '@') {
            PixelTanks.user.world.map[l] += '0';
            PixelTanks.user.tank.x = q * 50;
            PixelTanks.user.tank.y = l * 50;
          } else if (p[q] == ' ') {
            PixelTanks.user.world.map[l] += '0';
          }
        }
      }
    }

    frame() {
      GUI.draw.translate(PixelTanks.offset, 0);

      GUI.draw.fillStyle = '#000000';
      GUI.draw.fillRect(-1000, -1000, 3500, 3500);
      GUI.draw.clearRect(0, 0, 500, 500);

      GUI.draw.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack].floor, 0, 0, 1500, 1500);

      PixelTanks.user.tank.draw();

      var l = 0;
      while (l<this.ai.length) {
        this.ai[l].draw();
        l++;
      }

      l = 0;
      while (l<this.b.length) {
        this.b[l].draw();
        l++;
      }

      l = 0;
      while (l<this.b.length) {
        this.b[l].drawHealth();
        l++;
      }

      l = 0;
      while (l<this.s.length) {
        this.s[l].draw([{
          u: sessionStorage.username,
          col: '#0000FF',
        }, {
          u: 'Eval',
          col: '#FF0000',
        }]);
        l++;
      }

      var l = 0;
      while (l < this.b.length) {
        if (this.b[l].damagedRecent) {
          GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(b[l].x * 50, b[l].y * 50 + 50, 50, 5);
          GUI.draw.fillStyle = 'blue';
          GUI.draw.fillRect(b[l].x * 50 + 2, b[l].y * 50 + 51, 46 * b[l].health / b[l].maxHealth, 3);
        }
        l++;
      }


      GUI.draw.fillStyle = '#ffffff';
      GUI.draw.globalAlpha = .4;

      if (PixelTanks.user.tank.CanBoost) GUI.draw.globalAlpha = .8;
      GUI.draw.fillRect(100, 450, 50, 50);
      GUI.draw.globalAlpha = .4;

      if (PixelTanks.user.tank.CanToolkit) GUI.draw.globalAlpha = .8;
      GUI.draw.fillRect(183, 450, 50, 50);
      GUI.draw.globalAlpha = .4;

      if (PixelTanks.user.tank.canPlaceScaffolding) GUI.draw.globalAlpha = .8;
      GUI.draw.fillRect(266, 450, 50, 50);
      GUI.draw.globalAlpha = .4;

      if (PixelTanks.user.tank.canFireFlashbang) GUI.draw.globalAlpha = .8;
      GUI.draw.fillRect(349, 450, 50, 50);

      GUI.draw.globalAlpha = 1;

      GUI.draw.fillStyle = '#ffffff';
      GUI.draw.fillText(PixelTanks.userData.boosts, 135, 470);
      GUI.draw.fillText(PixelTanks.userData.toolkits, 183 + 35, 470);
      GUI.draw.fillText(PixelTanks.userData.blocks, 266 + 35, 470);
      GUI.draw.fillText(PixelTanks.userData.flashbangs, 349 + 35, 470);

      //GUI.draw.drawImage(coins, 0, 0);
      GUI.draw.fillText(PixelTanks.user.world.coins, 70, 40);

      if (this.level == 1) {
        GUI.draw.textAlign = 'center';
        GUI.draw.fillText('Welcome To Pixel Tanks!', 4 * 50, 26 * 50);
        GUI.draw.fillText('These Are Weak Blocks. Shoot to break them.', 11 * 50, 25.5 * 50);
        GUI.draw.fillText('Strong Blocks. Harder to Break.', 21 * 50, 25.5 * 50);
        GUI.draw.fillText('Invincible Blocks.', 21 * 50, 27 * 50);
        GUI.draw.fillText('Your first enemy awaits. Use right click(missle) and F(shield).', 17 * 50, 15 * 50);
        GUI.draw.fillText('Destroy all enemies to win!', 3 * 50, 12 * 50);
      }
    }

    intervals() {
      this.fps = 60;
      this.i.push(setInterval(this.frame.bind(this), 1000/this.fps));
      //this.i.push(setInterval(this.tick.bind(this), 1000/120));
    }
  }

class Block {
  constructor(x, y, health, type, host) {
    this.x = x;
    this.y = y;
    this.m = health; // max health
    this.h = health; // health
    this.t = type; // type
    this.ho = host; // host
    this.s = false; // show health bar
    this.c = true; // collision
    if (type === 'spike') {
      this.c = false;
    }
  }

  damage(s) {
    this.h -= s.damage;

    if (this.h !== Infinity) {
      this.s = true;
      clearTimeout(this.bar);
      this.bar = setTimeout(function() {
        this.s = false;
      }.bind(this), 3000);
    }

    if (this.h <= 0) {
      this.destroy();
    }
  }

  draw() {
    GUI.draw.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack][this.t], this.x, this.y, 50, 50);
  }

  drawHealth() {
    if (this.s) {
      GUI.draw.fillStyle = '#000000';
      GUI.draw.fillRect(this.x, this.y + 50, 50, 5);
      GUI.draw.fillStyle = 'blue';
      GUI.draw.fillRect(this.x + 2, this.y + 51, 46 * this.h / this.m, 3);
    }
  }

  destroy() {
    this.ho.b.splice(this.ho.b.indexOf(this), 1);
  }
}

  window.oncontextmenu = () => {
    return false;
  }
  window.onload = () => {
    PixelTanks.start();
  }
  window.addEventListener('resize', GUI.resize);
//})();
