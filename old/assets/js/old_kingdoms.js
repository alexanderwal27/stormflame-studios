'use strict';
window.onerror = function(a, b, c) {
  alert(a+' '+b+' '+c);
}

var scene = new THREE.Scene();
var loader = new GLTFLoader();
var camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000);
//var canvas = document.getElementById('canvas');
//var draw = canvas.getContext('2d');

// Make Canvas Cover entire screen
/*canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
canvas.style.display = 'none';
canvas.style.top = '0px';
canvas.style.left = '0px';
canvas.style.background = 'transparent';*/

// Game Background
scene.background = new THREE.Color('skyblue');

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.getElementById('body').appendChild( renderer.domElement );

const material = new THREE.MeshBasicMaterial({ color: 'green'});
const geometry = new THREE.BoxGeometry();
const cube = new THREE.Mesh(geometry, material);
const edges = new THREE.EdgesGeometry(geometry);
const lines = new THREE.LineSegments(edges, new THREE.LineBasicMaterial({ color: '#000000'}));
const spotLight = new THREE.SpotLight( 0xffffff );
spotLight.position.set( 100, 1000, 100 );

spotLight.castShadow = true;

spotLight.shadow.mapSize.width = 1024;
spotLight.shadow.mapSize.height = 1024;

spotLight.shadow.camera.near = 500;
spotLight.shadow.camera.far = 4000;
spotLight.shadow.camera.fov = 30;

scene.add( spotLight );
scene.add(lines);
scene.add(cube);

const controls = new PointerLockControls(camera, renderer.domElement);
document.addEventListener('click', function() {
  controls.lock();
})

camera.position.y = 40;
camera.lookAt(0, 0);
cube.scale.x = 200;
cube.scale.z = 200;
lines.scale.x = 200;
lines.scale.z = 200;

function gravity(y) {
  var results = [];
  if (y == 1.1) {
    results[0] = true;
  } else if (y < 1.1) {
    results[0] = true;
    results[1] = 1.1;
  } else {
    results[0] = false;
  }
  return results;
}
class Player {
  constructor(x, y, z) {
    this.update = this.update.bind(this);
    this.camera = camera;
    this.controls = controls;
    this.camera.position.x = x;
    this.camera.position.y = y;
    this.camera.position.z = z;
    this.x   = x;
    this.y = y;
    this.z = z;
    this.i = [];
    this.t = [];
    this.i.push(setInterval(this.update, 20));
    this.intervals = [];
    this.helpers = [];
    this.addListeners();
    this.dy = 0;
  }
  update() {
    var results = gravity(this.camera.position.y);
    if (results[0]) {
      if (results[1]) this.camera.position.y = results[1];
      if (this.dy < 0) {
        this.dy = 0;
      }
    } else {
      this.dy -= .01;
    }
    this.camera.position.y += this.dy;
  }
  keydown(e) {
    if (user.player.helpers[e.keyCode] != true) {
      user.player.intervals[e.keyCode] = window.setInterval(user.player.move, 15, e.keyCode);
    }
    user.player.helpers[e.keyCode] = true;
  }
  keyup(e) {
    window.clearInterval(user.player.intervals[e.keyCode]);
    user.player.helpers[e.keyCode] = false;
    user.player.intervals[e.keyCode] = false;
  }
  move(key) {
    if (key == 87) {
      controls.moveForward(.2);
    } else if (key == 83) {
      controls.moveForward(-.2);
    } else if (key == 65) {
      controls.moveRight(-.2);
    } else if (key == 68) {
      controls.moveRight(.2);
    } else if (key == 16) {
    } else if (key == 32 && gravity(user.player.camera.position.y)[0]) {
      user.player.dy = .5;
    }
    user.player.x = user.player.camera.position.x;
    user.player.y = user.player.camera.position.y;
    user.player.z = user.player.camera.position.z;
  }
  addListeners() {
    document.addEventListener('keydown', this.keydown);
    document.addEventListener('keyup', this.keyup)
  }
  removeListeners() {
    document.removeEventListener('keydown', user.player.keydown);
    document.removeEventListner('keyup', user.player.keyup);
  }
  inventory() {
  }
}

class Slime {
  constructor(x, y, z, scale) {
    this.update = this.update.bind(this);
    this.attack = this.attack.bind(this);
    this.generateNewAttack = this.generateNewAttack.bind(this);
    this.destroy = this.destroy.bind(this);
    this.target = {};
    this.xspeed = 0;
    this.zspeed = 0;
    this.i = []; // intervals
    this.t = []; // timeouts
    this.dy = 0;
    this.hopDistance = 10;
    this.hopHeight = .3;
    var context = this;
    var slime;
    loader.load('assets/games/objects/slime.glb', function(object) {
      slime = object.scene;
      slime.scale.set(scale, scale, scale);
      slime.position.y = x;
      slime.position.x = y;
      slime.position.z = z;
      scene.add(slime);
      context.object = slime;
      context.generateNewAttack();
      context.i.push(setInterval(context.update, 20));
    }, undefined, function(err) {
      alert(err);
    })
  }
  generateNewAttack() {
    var xd = user.player.x - this.object.position.x;
    var zd = user.player.z - this.object.position.z;
    var distance = Math.sqrt(xd*xd+zd*zd);
    var ratio = 1;
    /*if (this.hopDistance > distance) {
      ratio = 1;
    } else {
      ratio = this.hopDistance/distance;
    }*/
    var x = (xd+this.object.position.x)*ratio;
    var z = (zd+this.object.position.z)*ratio;
    this.target.x = x;
    this.target.z = z;
    var xdis = this.target.x - this.object.position.x;
    var zdis = this.target.z - this.object.position.z;
    this.frames = 138;
    this.xspeed = xdis/this.frames;
    this.zspeed = zdis/this.frames;
    this.dy = this.hopHeight;
    text2.innerHTML = 'USER('+user.player.x+', '+user.player.z+') Target('+this.target.x+', '+this.target.z+') - Pos('+this.object.position.x+', '+this.object.position.z+')  DIS('+xdis+', '+zdis+')';
    //text2.innerHTML = '(x^2+z^2)='+new String(Math.sqrt(x^2+z^2))+' - z='+travelDistance;
  }
  rotate() {
    var a = angle(user.player.camera.position.x, user.player.camera.position.z, this.object.position.x, this.object.position.z)
    this.object.rotation.y = (-a*Math.PI/180)+80;
  }
  attack() {
  }
  update() {
    this.rotate();
    var results = gravity(this.object.position.y);
    if (results[0]) {
      if (results[1]) this.object.position.y = results[1];
      if (this.dy < 0) {
        this.dy = 0;
      }
      this.xspeed = 0;
      this.zspeed = 0;
      setTimeout(function(context) {
        context.generateNewAttack();
      }, 1000, this);
    } else {
      this.dy -= .01;
    }
    this.object.position.y += this.dy;
    this.object.position.x += this.xspeed;
    this.object.position.z += this.zspeed;
  }
  destroy() {
    var l = 0;
    while (l<this.i.length) {
      clearInterval(this.i);
      l++;
    }
    var l = 0;
    while (l<this.t.length) {
      clearTimeout(this.t);
      l++;
    }
  }
}

class World {
  constructor() {
    this.i = [];
    this.t = [];
    this.e = [];
    this.t.push(setTimeout(function() {
      user.world.spawn('slime');
    }, 2000))
  }
  spawn(id) {
    if (id == 'slime') {
      var entity = new Slime(0, 0, 0, .1);
      this.e.push(entity);
      return entity;
    }
  }
}

var user = {
  player: new Player(0, 20, 0),
  world: new World(),
}

function angle(cx, cy, ex, ey) {
  var dy = ey - cy;
  var dx = ex - cx;
  var theta = Math.atan2(dy, dx); // range (-PI, PI]
  theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
  //if (theta < 0) theta = 360 + theta; // range [0, 360)
  return theta;
}

function animate() {
  user.player.x = user.player.camera.position.x;
  user.player.y = user.player.camera.position.y;
  user.player.z = user.player.camera.position.z;
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
}
animate();


var text2 = document.createElement('div');
text2.style.position = 'absolute';
//text2.style.zIndex = 1;    // if you still don't see the label, try uncommenting this
text2.style.width = 100;
text2.style.height = 100;
text2.style.backgroundColor = "blue";
text2.innerHTML = "hi there!";
text2.style.top = 200 + 'px';
text2.style.left = 200 + 'px';
document.body.appendChild(text2);
